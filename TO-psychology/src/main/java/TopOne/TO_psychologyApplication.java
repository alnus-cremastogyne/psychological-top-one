package TopOne;

import org.apache.catalina.core.ApplicationContext;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;


@MapperScan("TopOne.UserSystem.mapper,TopOne.ListenerSystem.mapper")
@SpringBootApplication
@EnableScheduling // 启用定时任务功能
public class TO_psychologyApplication {
    public static void main(String[] args)
    {
        SpringApplication.run(TO_psychologyApplication.class, args);
    }

}

package TopOne.ws;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;



/**
 * @author websocket服务
 */
@ServerEndpoint(value = "/imserver/{userId}")
@Component
public class WebSocketServer {

    private static final Logger log = LoggerFactory.getLogger(WebSocketServer.class);

    /**
     * 记录当前在线连接数
     */
    public static final Map<String, Session> sessionMap = new ConcurrentHashMap<>();
    //public static final Map<String, Session> UserMap = new ConcurrentHashMap<>();这里没有需要知道对方名字的需求 所以不需要加 需要再加

    /**
     * <<<<<<< HEAD
     * 设置为静态的 公用一个消息map ConcurrentMap为线程安全的map  HashMap不安全
     */
    //这里的messageMap存的是某用户已经离线 他离线后收到的消息的集合 所以这里的key是接收者的key
    private static ConcurrentMap<String, List<String>> messageMap = new ConcurrentHashMap<>();

    /**
     *
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("userId") String userId) {
        sessionMap.put(userId, session);

//        stringRedisTemplate.opsForList().

        log.info("有新用户加入，userId={}, 当前在线人数为：{}", userId, sessionMap.size());
        JSONObject result = new JSONObject();
        JSONArray array = new JSONArray();
        result.set("users", array);
        for (Object key : sessionMap.keySet()) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.set("userId", key);
            // {"userId": "aysgduiehfiuew", "userId": "admin"}
            array.add(jsonObject);
        }
        //这里得到的是该用户的历史记录map userMessage
        List<String> userMessage = messageMap.get(userId);
        //载入历史记录  这个过程相当于重新把消息发给自己
        if (userMessage!=null) {
            for (int i = userMessage.size() - 1; i >= 0; i--) {
                String message = userMessage.get(i);
                //这里的session的作用是告诉sendMessage发给谁 这里是要加载自己错过的历史消息
                // 所以是把历史记录发给自己 所以toSession填的是自己的session
                this.sendMessage(message, session);
//                Thread.sleep(10000);
            }
            messageMap.remove(userId);
        }
//        {"users": [{"userId": "zhang"},{ "userId": "admin"}]}
        sendAllMessage(JSONUtil.toJsonStr(result));  // 后台发送消息给所有的客户端
    }
    /**
     * 服务端发送消息给客户端
     */
    private void sendMessage(String message, Session toSession) {
        try {
            log.info("服务端给客户端[{}]发送消息{}", toSession.getId(), message);
            toSession.getBasicRemote().sendText(message);
            //String from = JSONUtil.parseObj(message).getStr("from");
            if (!messageMap.get(toSession.getId()).isEmpty()) {
                List<String> list = messageMap.get(toSession.getId());
                log.info("有待发送的消息，继续存储");
                list.add(message);
                //toSession是被发送者的id
                messageMap.put(toSession.getId(), list);
                return;
            } else {
                List<String> list = new ArrayList<>();
                //该用户发的离线消息的集合
                list.add(message);
                messageMap.put(toSession.getId(), list);
                log.info("用户不在线保存信息");
                return;
            }
        } catch (Exception e) {
            log.error("服务端发送消息给客户端失败", e);
        }
        //        {"users": [{"userId": "zhang"},{ "userId": "admin"}]}
    }


    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose(Session session, @PathParam("userId") String userId) {
        sessionMap.remove(userId);
        log.info("有一连接关闭，移除username={}的用户session, 当前在线人数为：{}", userId, sessionMap.size());
    }

    /**
     * 收到客户端消息后调用的方法
     * 后台收到客户端发送过来的消息
     * onMessage 是一个消息的中转站
     * 接受 浏览器端 socket.send 发送过来的 json数据
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session, @PathParam("userId") String userId) {
        log.info("服务端收到用户username={}的消息:{}", userId, message);
        JSONObject obj = JSONUtil.parseObj(message);
        String toUserId = obj.getStr("to"); // to表示发送给哪个用户，比如 admin
        String text = obj.getStr("text"); // 发送的消息文本  hello

        //建立一个数组 把每一次的都装进去 然后下面

        //TODO 这里要写 一个缓存历史记录的方法来处理 除了test123 是用于心跳的 就不用缓存
        if(!toUserId.equals("test123")){
            Session toSession = sessionMap.get(toUserId); // 根据 to userId来获取 session，再通过session发送消息文本
            if (toSession != null) {
                // 服务器端 再把消息组装一下，组装后的消息包含发送人和发送的文本内容
                // {"from": "zhang", "text": "hello"}
                JSONObject jsonObject = new JSONObject();
                jsonObject.set("from", userId);  // from 是 zhang
                jsonObject.set("text", text);  // text 同上面的text
                this.sendMessage(jsonObject.toString(), toSession);
                log.info("发送给用户username={}，消息：{}", toUserId, jsonObject.toString());
            } else {
                log.info("发送失败，未找到用户username={}的session", toUserId);
            }
        }
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.error("发生错误");
        error.printStackTrace();
    }

    /**
     * 服务端发送消息给所有客户端
     */
    private void sendAllMessage(String message) {
        try {
            for (Session session : sessionMap.values()) {
                log.info("服务端给客户端[{}]发送消息{}", session.getId(), message);
                session.getBasicRemote().sendText(message);
            }
        } catch (Exception e) {
            log.error("服务端发送消息给客户端失败", e);
        }
    }
}


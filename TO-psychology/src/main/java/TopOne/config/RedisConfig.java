package TopOne.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import java.util.Collections;

/**
 * @author suze
 * @date 2023/10/26
 * @time 13:31
 **/
@Configuration
public class RedisConfig {

//    #  内网链接：
//            #  云MySQL：ob-5cq9970rorls.db.antcloud.local:3306
//            #  云Redis：proxy-43c154b4947f433d8574302c24f43eff.redis.antcloud.local
//#  外网：
//            #  sql：ob-5cq9970rorls.db.cloudob.cn:3306
//            #  Redis：proxy-ddbe43f85cd24235a7a83b63e196602f.redis.cloudrun.cloudbaseapp.cn

    @Value("${spring.redis.host}")
    private String redisHost="proxy-43c154b4947f433d8574302c24f43eff.redis.antcloud.local";

    @Value("${spring.redis.port}")
    private int redisPort=6379;

    @Value("${spring.redis.password}")
    private String redisPassword="SU791616";

    @Bean
    public LettuceConnectionFactory redisConnectionFactory() {
        RedisStandaloneConfiguration config = new RedisStandaloneConfiguration(redisHost, redisPort);
        config.setPassword(RedisPassword.of(redisPassword));
        return new LettuceConnectionFactory(config);
    }

    @Bean
    public RedisTemplate<String, String> redisTemplate(LettuceConnectionFactory connectionFactory) {
        RedisTemplate<String, String> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory);
        template.setDefaultSerializer(new StringRedisSerializer());
        return template;
    }

}

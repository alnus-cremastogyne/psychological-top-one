package TopOne.config;

import TopOne.utils.LoginInterceptor;
import TopOne.utils.RefreshTokeninterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;
/*/**
 *
 *@author suze
 *@date 2023-10-25
 *@time 15:13
 *
 **/
@Configuration
public class MvcConfig implements WebMvcConfigurer {
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Override
    //添加拦截器  InterceptorRegistry registry 拦截器的注册器  excludePathPatterns排除不需要的拦截的路径
    // 只要跟登录无关就不需要拦截  拦截器的作用只是校验登录状态
    public void addInterceptors(InterceptorRegistry registry) {

            registry.addInterceptor(new LoginInterceptor())
                    .excludePathPatterns(
                    "/index/**",
                    "/user/wechat/login",
                    "/user/zfb/login",
                     "/user/Psychological",
                     "/Questionnaire/info",
                     "/Listener/info/log"

            ).order(1);
//        order是设置先后
//        刷新token的拦截器
        registry.addInterceptor(new RefreshTokeninterceptor(stringRedisTemplate)).addPathPatterns("/**").order(0);
    }

}

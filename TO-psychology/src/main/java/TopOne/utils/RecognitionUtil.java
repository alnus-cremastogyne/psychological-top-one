package TopOne.utils;

import cn.hutool.json.JSONUtil;
import com.alibaba.dashscope.audio.asr.recognition.Recognition;
import com.alibaba.dashscope.audio.asr.recognition.RecognitionParam;
import com.alibaba.dashscope.audio.asr.recognition.timestamp.Sentence;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import static TopOne.utils.SystemConstants.DASHSCOPE_API_KEY;

public class RecognitionUtil {
    public static String  Recognition(InputStream in) {
        // 用户可忽略url下载文件部分，可以直接使用本地文件进行相关api调用进行识别
//        String exampleWavUrl =
//                "https://dashscope.oss-cn-beijing.aliyuncs.com/samples/audio/paraformer/hello_world_female2.wav";
        try {
            //InputStream in = new URL(exampleWavUrl).openStream();
            Files.copy(in, Paths.get("asr_example.wav"), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            System.out.println("error: " + e);
            System.exit(1);
        }
        System.out.println("111111");
        // 创建Recognition实例
        Recognition recognizer = new Recognition();
        // 创建RecognitionParam，请在实际使用中替换真实apiKey
        RecognitionParam param =
                RecognitionParam.builder()
                        .model("paraformer-realtime-v1")
                        .format("wav")
                        .sampleRate(16000)
                        .apiKey(DASHSCOPE_API_KEY)
                        .build();
        // 直接将结果保存到script.txt中
        try (FileOutputStream fos = new FileOutputStream("asr_result.txt")) {
            String result = recognizer.call(param, new File("asr_example.wav"));
            System.out.println(result);
            fos.write(result.getBytes());
            JSONObject jsonObject = JSON.parseObject(JSONUtil.toJsonStr(result));
            JSONArray sentences = jsonObject.getJSONArray("sentences");
            for (int i = 0; i < sentences.size(); i++) {
                JSONObject sentence = sentences.getJSONObject(i);
                String text = sentence.getString("text");
                return text;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

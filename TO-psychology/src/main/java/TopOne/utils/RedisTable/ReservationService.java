package TopOne.utils.RedisTable;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/***
 * @title ReservationService
 * @author SUZE
 * @Date 19:26
 **/
@Service
public class ReservationService {
    /**
     *description<时间判定  保障预约时间在现在时间之后>

         * @return
         * @author SUZE
         * @time 2024/1/5-17:32
         */

    private final StringRedisTemplate stringRedisTemplate;
    LocalDate currentDate = LocalDate.now();
    //private String dateStr = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    public ReservationService(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }


    public boolean isReserved(String listenerId, String time,String dateStr) {
        String hashKey = "schedule:" + dateStr + ":"+listenerId;
        Boolean reserved = stringRedisTemplate.opsForHash().hasKey(hashKey, time);
        return reserved != null && reserved; // 如果键不存在或值为false，则返回false
    }


    //要规定好加入进来的time只能是由最小粒度30分钟来组成的
    //诸如9:30-10:00此类 最小单位是30分钟 从8:00开始
    public void Reserved(String listenerId, String time,String dateStr){
        String hashKey = "schedule:" + dateStr + ":"+listenerId;
        stringRedisTemplate.opsForHash().put(hashKey,time, "true");
    }
    /**
     *description<取消方法>
    [listenerId, time, dateStr]
         * @return void
         * @author SUZE
         * @time 2023/12/20-20:45
         */
    public void UnReserved(String listenerId, String time,String dateStr){
        String hashKey = "schedule:" + dateStr + ":"+listenerId;//schedule:2024-01-15:2
        stringRedisTemplate.opsForHash().put(hashKey,time, "false");
    }

    public boolean isOverThatTime(String dateStr) {
        // 将日期字符串转化为Date变量
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
        Date date;
        try {
            date = dateFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
            return false; // 转换失败，返回false或抛出异常，根据情况自行处理
        }
        // 判断时间是否小于当前时间
        Calendar currentTime = Calendar.getInstance(); // 获取当前时间
        Calendar targetTime = Calendar.getInstance();
        targetTime.setTime(date); // 设置目标时间为转换后的日期变量

        if (targetTime.before(currentTime)) {
            // 时间小于当前时间，表示dateStr所代表的时间 此时已经到达
            return true;
        }
        return false;
    }

    public JSONArray GetUnReserved(String listenerId, String dateStr) {
        // 根据listenerId和dateStr查对应的空闲状态表
        String hashKey = "schedule:" + dateStr + ":" + listenerId;
        HashOperations<String, String, String> hashOperations = stringRedisTemplate.opsForHash();

        // 创造从现在开始的最近的整数时间单位，例如从08:00以30分钟为一个间隔单位开始，一直到24:00，封装到List<String>里面
        List<String> timeList = createTimeList();

        // 遍历循环时间列表，检查对应的空闲状态表看是否被预约，被预约就移除掉
        for (String time : timeList) {
            String reservedStatus = hashOperations.get(hashKey, time);
            if (reservedStatus != null && reservedStatus.equals("true")) {
                timeList.remove(time);
            }
        }
        // 返回未被预约的时间列表
        return JSONUtil.parseArray(timeList);
    }

    private List<String> createTimeList() {
        List<String> timeList = new ArrayList<>();
        LocalTime startTime = LocalTime.of(8, 0); // 开始时间 08:00
        LocalTime endTime = LocalTime.of(24, 0); // 结束时间 24:00
        int intervalMinutes = 30; // 时间间隔，30分钟

        LocalTime currentTime = LocalTime.now();
        if (currentTime.isBefore(startTime)) {
            currentTime = startTime;
        }

        while (currentTime.isBefore(endTime)) {
            timeList.add(currentTime.toString());
            currentTime = currentTime.plusMinutes(intervalMinutes);
        }

        return timeList;
    }
}
package TopOne.utils.RedisTable;
import TopOne.ListenerSystem.service.ListenerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.List;
import java.util.concurrent.TimeUnit;

/***
 * @title ScheduleCleanupTask
 * @author SUZE
 * @Date 19:19
 **/
@Component
public class ScheduleCleanupTask {
    @Autowired
    private ListenerService listenerService;
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public ScheduleCleanupTask(StringRedisTemplate stringRedisTemplate, ListenerService listenerService) {
        this.stringRedisTemplate = stringRedisTemplate;
        this.listenerService = listenerService;
    }


    //每一次启用服务器就自动执行这个方法(PostConstruct后构造器) 相当于初始化
    // 原理：在构造ScheduleCleanupTask这个Bean的时候他就会自动初始化
    //在测试的时候记得关掉  丢到 小程序里面记得开
//    @PostConstruct
//    public void init() {
//        CreateBookTable();
//    }
    @Scheduled(cron = "0 0 0 * * 1") // 每周执行一次，周一凌晨00:00
    public void CreateBookTable() {
        // 获取当前日期
        LocalDate currentDate = LocalDate.now();
        //String currentDateStr = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        List<String> listenerIdList = listenerService.getListenerIdList();
        for (String listenerid : listenerIdList) {
            //每一个倾听者都 创建七天的预约记录表
            for (int i = 0; i < 7; i++) {
                LocalDate date = currentDate.plusDays(i);
                String dateStr = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                String hashKey = "schedule:" + dateStr + ":"+listenerid; // 表名带有listenerid
                stringRedisTemplate.opsForHash().put(hashKey, "time", "isReserved");
                stringRedisTemplate.expire(hashKey,7, TimeUnit.DAYS);
            }
        }
    }

}
package TopOne.utils;

import cn.hutool.core.util.BooleanUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import static TopOne.utils.RedisConstants.*;

/**
 * @author suze
 * @date 2023/10/28
 * @time 21:00
 **/
@Component
public class CacheClient {
    private StringRedisTemplate stringRedisTemplate;

    public CacheClient(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }
    public void set(String key, Object value, long time, TimeUnit  timeUnit){
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(value),time,timeUnit);
    }
    public void setLogicalExpire(String key, Object value, long time, TimeUnit  timeUnit){
        //设置逻辑过期时间
        TopOne.utils.RedisData redisData = new TopOne.utils.RedisData();
        redisData.setData(value);
        redisData.setExpireTime(LocalDateTime.now().plusSeconds(timeUnit.toSeconds(time)));
        //写入Redis
        stringRedisTemplate.opsForValue().set(key, JSONUtil.toJsonStr(redisData));
    }
    //这里使用泛型R和ID都是不确定的类型  (有参数有返回值的函数在java里就是Function)
    public  <R,ID>R queryWithPassThrough(String keyPrefix, ID id, Class<R> type,
                                         Function<ID,R> dbfunction,long time, TimeUnit  timeUnit){
        //1.从Redis查询id  这里使用的数据结构可以是String也可以是hash  若是查询不到就为空了 CACHE_SHOP_KEY就是"cache:shop:"
        String json = stringRedisTemplate.opsForValue().get(keyPrefix + id);
        //2.判断是否 在字符串意义上是否为空
        if (StrUtil.isNotBlank(json)) {
            //3.存在直接返回

            return  JSONUtil.toBean(json, type);
        }
        //3.判断是否命中写入redis中的null
        if(json != null){
            return null;
        }
        //这里的数据库查询我们的数据不知道要查询哪一种类型所以需要调用者传递一个泛型函数Function
        R r = dbfunction.apply(id);
        //5.数据库中不存在 返回报错
        if (r == null){
            //空值null写入redis
            stringRedisTemplate.opsForValue().set(keyPrefix + id, null,CACHE_NULL_TTL, TimeUnit.MINUTES);
            return null;
        }
        //6.数据库中存在  写入Redis  并返回
        this.set(keyPrefix+id, r, time, timeUnit);

        return r;
    }
    //搭建一个线程池
    private static final ExecutorService executor = Executors.newFixedThreadPool(10);
    //解决缓存穿透问题
    public <R,ID>R queryWithLocalExpire(String keyPrefix,ID id, Class<R> type,
                                         Function<ID,R> dbfunction,long time, TimeUnit timeUnit){

        //1.先查询Redis
        String json = stringRedisTemplate.opsForValue().get(keyPrefix);

        //2.判断是否存在
        if (StrUtil.isBlank(json)){
            //不存在 直接返回空
            return null;
        }
        //3.命中 先反序列化为对象
        RedisData redisData = JSONUtil.toBean(json, RedisData.class);
        R r = JSONUtil.toBean((JSONObject) redisData.getData(), type);
        //4.查询是否过期
        if (redisData.getExpireTime().isAfter(LocalDateTime.now())){
            //5.未过期 直接返回
            return r;
        }
        //6.过期 重构缓存
        String lockKey = LOCK_SHOP_KEY + id;

        //6.1获得互斥锁
        // 6.2判断是否成功
        if(tryLock(lockKey)){
            //6.3成功 开启新线程 实现重构缓存
            executor.submit(() ->{
                //重建缓存
                try {
                    //先查数据库
                    R r1 = dbfunction.apply(id);
                    //再写入redis
                    this.setLogicalExpire(keyPrefix,r1,time,timeUnit);

                } catch (Exception e) {
                    throw new RuntimeException(e);
                }finally {
                    //关锁
                    unLock(lockKey);
                }

            });

        }
        //6.4失败 直接返回旧数据
        return r;

    }
    private boolean tryLock(String key){
        //自定义互斥锁  将申请锁的结果返回
        Boolean flag = stringRedisTemplate.opsForValue().setIfAbsent(key, "1", 10L, TimeUnit.SECONDS);
        return BooleanUtil.isTrue(flag);

    }
    //释放锁
    private void unLock(String key){
        stringRedisTemplate.delete(key);

    }

}

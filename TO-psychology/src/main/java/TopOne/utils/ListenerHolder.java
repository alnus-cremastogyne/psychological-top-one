package TopOne.utils;

import TopOne.ListenerSystem.entity.Listener;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;

public class ListenerHolder
{
    private static final ThreadLocal<Listener> tl = new ThreadLocal<>();

    public static void saveListener(Listener listener){
        tl.set(listener);
    }

    public static Listener getListener(){
        Listener listener = tl.get();
        if (listener==null){
            return null;
        }
        if (listener.getFeatureTags()!=null){
            listener.setFeatureTagArray(JSONUtil.parseArray(listener.getFeatureTags()));
        }
        return listener;
    }

    public static void removeListener(){
        tl.remove();
    }
}

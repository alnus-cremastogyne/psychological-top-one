package TopOne.utils;

import TopOne.dto.Result;
import TopOne.UserSystem.entity.Ws.WebResult;

public class MessageUtil {
    public static Result getMessage(boolean isSystemMessage, String fromName, Object message){
        WebResult result =new  WebResult();
        result.setMessage(message);
        result.setSystem(isSystemMessage);
        if (fromName!=null){
            result.setFormName(fromName);
        }
        return Result.ok(result);
    }
}

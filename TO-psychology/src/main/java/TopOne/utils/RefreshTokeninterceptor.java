package TopOne.utils;

import TopOne.ListenerSystem.entity.Listener;
import TopOne.UserSystem.entity.User.User;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

import static TopOne.utils.RedisConstants.LOGIN_LISTENER_KEY;
import static TopOne.utils.RedisConstants.LOGIN_USER_KEY;

/*/**
 *@author suze
 *@date 2023-10-25
 *@time 15:23
 **/
public class RefreshTokeninterceptor implements HandlerInterceptor {

    //而MvcConfig中使用了 LoginInterceptor 所以我们要去到MvcConfig进行注入
    private StringRedisTemplate stringRedisTemplate;
    //因为这个类不是spring boot构建的，而是手动创建的类，所以依赖注入不能用注解来注入，要我们手动使用构造函数来注入这个依赖
    public RefreshTokeninterceptor(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token2=request.getHeader("token2");
        String ListenerKey = LOGIN_LISTENER_KEY + token2;
        //这里的倾听者信息是在倾听者登录的函数里面把倾听者信息录入进去
        String LisStr = stringRedisTemplate.opsForValue().get(ListenerKey);
        if(LisStr== null || LisStr.isEmpty()){
            System.err.println("倾听者token为空");
        }
        else {
            Listener listener = JSON.parseObject(LisStr, Listener.class);
            ListenerHolder.saveListener(listener);
            stringRedisTemplate.expire(ListenerKey,15, TimeUnit.MINUTES);
            return true;
        }
        //获取请求头中的token  在前端代码中详见authorization
        String token = request.getHeader("token");
        if(StrUtil.isBlank(token)){//判断是否为空
            System.err.println("token为空");
            return  true;
        }
        // 基于token获取Redis用户
        String key =LOGIN_USER_KEY+token;
        String userstr = stringRedisTemplate.opsForValue().get(key);
        //System.err.println("基于token获取Redis用户:"+userstr);
        //判断用户是否存在  不存在的话就查询是否是倾听者的情况
        if(userstr== null || userstr.isEmpty()){
            System.err.println("用户为空");
            return  true;
        }
        // 将查询到的user的json字符串转化为user对象
        User user = JSON.parseObject(userstr, User.class);
        //存在 保存用户信息到TheadLocal
        UserHolder.saveUser(user);
        System.out.println("保存用户"+user.getOpenId()+"信息到TheadLocal了");
        //刷新token有效期
        stringRedisTemplate.expire(key,15, TimeUnit.MINUTES);
        //放行
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        //移除用户
        UserHolder.removeUser();
        ListenerHolder.removeListener();
    }
}

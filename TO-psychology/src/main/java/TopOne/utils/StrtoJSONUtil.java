package TopOne.utils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class StrtoJSONUtil {
    public static JSONObject convertToJSON(String input) {
        JSONObject jsonObject = new JSONObject();
        String[] pairs = input.split(", ");
        for (String pair : pairs) {
            String[] keyValue = pair.split(": ");
            if (keyValue.length == 2) {
                String key = keyValue[0].trim();
                String value = keyValue[1].replaceAll("\"", "");
                jsonObject.put(key, value);
            }
        }


        return jsonObject;
    }

    public static JSONObject convertToJSONObject(String input) {
        JSONObject jsonObject = new JSONObject();
        String[] pairs = input.split(",\n");
        for (String pair : pairs) {
            String[] keyValue = pair.split(": ");
            if (keyValue.length == 2) {
                String key = keyValue[0].trim().replaceAll("\"", "");
                String value = keyValue[1].trim().replaceAll("(^\"|\"$)", "").replaceAll("\\\\", "");
                jsonObject.put(key, value);
            }
        }
        return jsonObject;
    }
}


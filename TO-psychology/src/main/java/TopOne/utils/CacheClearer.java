package TopOne.utils;

import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CacheClearer {
    private final SqlSessionFactory sqlSessionFactory;

    @Autowired
    public CacheClearer(SqlSessionFactory sqlSessionFactory) {
        this.sqlSessionFactory = sqlSessionFactory;
    }

    public void clearCache(String cacheName) {
        sqlSessionFactory.getConfiguration().getCache(cacheName).clear();
    }
}

package TopOne.utils;

import TopOne.config.UnAuthorException;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginInterceptor implements HandlerInterceptor {
        @Override
        public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
            //1.判断是否需要拦截（ThreadLocal中是否有用户）
            if (UserHolder.getUser() == null&&ListenerHolder.getListener()==null) {
                System.out.println("拦截器报错啦！！！");
                //response.getHeader("erro");
                throw new UnAuthorException("用户未登录");
            }
            return true;
        }
}

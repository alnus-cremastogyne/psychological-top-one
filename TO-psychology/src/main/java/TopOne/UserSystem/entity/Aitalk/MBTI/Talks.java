package TopOne.UserSystem.entity.Aitalk.MBTI;

/**
 * @author suze
 * @date 2023/11/6
 * @time 11:56
 **/
public class Talks {
    private String answer = "[\n" +
            "  { id: 0, btype: \"true\", type: \"J\" },\n" +
            "  { id: 1, btype: \"false\", type: \"J\" },\n" +
            "  { id: 2, btype: \"true\", type: \"S\" },\n" +
            "  { id: 3, btype: \"true\", type: \"E\" },\n" +
            "  { id: 4, btype: \"false\", type: \"S\" },\n" +
            "  { id: 5, btype: \"false\", type: \"T\" },\n" +
            "  { id: 6, btype: \"false\", type: \"J\" },\n" +
            "  { id: 7, btype: \"true\", type: \"E\" },\n" +
            "  { id: 8, btype: \"true\", type: \"J\" },\n" +
            "  { id: 9, btype: \"true\", type: \"J\" },\n" +
            "  { id: 10, btype: \"false\", type: \"J\" },\n" +
            "  { id: 11, btype: \"false\", type: \"E\" },\n" +
            "  { id: 12, btype: \"true\", type: \"S\" },\n" +
            "  { id: 13, btype: \"true\", type: \"E\" },\n" +
            "  { id: 14, btype: \"false\", type: \"S\" },\n" +
            "  { id: 15, btype: \"false\", type: \"T\" },\n" +
            "  { id: 16, btype: \"true\", type: \"T\" },\n" +
            "  { id: 17, btype: \"false\", type: \"E\" },\n" +
            "  { id: 18, btype: \"true\", type: \"E\" },\n" +
            "  { id: 19, btype: \"true\", type: \"T\" },\n" +
            "  { id: 20, btype: \"true\", type: \"T\" },\n" +
            "  { id: 21, btype: \"false\", type: \"E\" },\n" +
            "  { id: 22, btype: \"true\", type: \"E\" },\n" +
            "  { id: 23, btype: \"false\", type: \"S\" },\n" +
            "  { id: 24, btype: \"true\", type: \"T\" },\n" +
            "  { id: 25, btype: \"false\", type: \"E\" },\n" +
            "]";
    private String options = "[\n" +
            "  { ans1: \"计划你要做什么和在什么时候做\", ans2: \"说去就去\", },\n" +
            "  { ans1: \"较为随兴所至的人\", ans2: \"较为有条理的人\", },\n" +
            "  { ans1: \"以事实为主的课程\", ans2: \"涉及理论的课程\", },\n" +
            "  { ans1: \"与人容易混熟\", ans2: \"比较沉静或矜持\", },\n" +
            "  { ans1: \"富于想象力的人\", ans2: \"现实的人\", },\n" +
            "//index=5\n" +
            "  { ans1: \"你的情感支配你的理智\", ans2: \"你的理智主宰你的情感\", },\n" +
            "  { ans1: \"凭兴所至行事\", ans2: \"按照计划行事\", },\n" +
            "  { ans1: \"容易让人了解\", ans2: \"难于让人了解\", },\n" +
            "  { ans1: \"合你心意\", ans2: \"令你感到束缚\", },\n" +
            "  { ans1: \"开始前小心组织计划\", ans2: \"边做边找须做什么\", },\n" +
            "  //index=10\n" +
            "  { ans1: \"顺其自然\", ans2: \"制定计划\", },//顺 字符错误\n" +
            "  { ans1: \"重视自我隐私的人\", ans2: \"非常坦率开放的人\", },\n" +
            "  { ans1: \"实事求是的人\", ans2: \"机灵的人\", },\n" +
            "  { ans1: \"客观的人和物\", ans2: \"观点和见解\", },//ei改\n" +
            "  { ans1: \"悟性，对概念的掌握\", ans2: \"练习，对事物的熟悉\", },//sn改\n" +
            "  //index=15\n" +
            "  { ans1: \"重视感情多于逻辑\", ans2: \"重视逻辑多于感情\", },\n" +
            "  { ans1: \"具分析力\", ans2: \"多愁善感\", },\n" +
            "  { ans1: \"一个人独处\", ans2: \"和别人在一起\", },\n" +
            "  { ans1: \"令你活力倍增\", ans2: \"常常令你心力憔悴\", },\n" +
            "  { ans1: \"思考\", ans2: \"感受\", },\n" +
            "  //index=20\n" +
            "  { ans1: \"条理性\", ans2: \"开放性\",},\n" +
            "  { ans1: \"有时感到郁闷\", ans2: \"常常乐在其中\", },\n" +
            "  { ans1: \"和别人容易混熟\", ans2: \"趋向自处一隅\", },\n" +
            "  { ans1: \"一个思想敏捷及非常聪颖的人\", ans2: \"实事求是，具丰富常识的人\", },\n" +
            "  { ans1: \"公事公办\", ans2: \"稍作通融\", }," +
            "  //index=25\n" +
            "  { ans1: \"要花很长时间才认识你\", ans2: \"用很短的时间便认识你\", },\n" +
            "  //index=26\n" +"               ]"
            ;//这里可以直接存在Redis或者MySQL或者存在本地 写个函数从本地读取这些数据
    private String ques = "[\n"+
            "                    \"当你要外出一整天，你会\",\n" +
            "                    \"你认为自己是一个\",\n" +
            "                    \"假如你是一位老师，你会选教\",\n" +
            "                    \"你通常\",\n" +
            "                    \"一般来说，你和哪些人比较合得来？\",\n" +
            "                    \"你是否经常让\",\n" +
            "                    \"处理许多事情上，你会喜欢\",\n" +
            "                    \"你是否\",\n" +
            "                    \"按照程序表做事，\",\n" +
            "                    \"当你有一份特别的任务，你会喜欢\",\n" +
            "                    \"在大多数情况下，你会选择\",\n" +
            "                    \"大多数人会说你是一个\",\n" +
            "                    \"你宁愿被人认为是一个\",\n" +
            "                    \"聚会中，你的兴趣和注意通常追随\",\n" +
            "                    \"哪种学习方式于你较有效？\",\n" +
            "                    \"你倾向\",\n" +
            "                    \"以下哪个词语更符合你\",\n" +
            "                    \"你喜欢花很多的时间\",\n" +
            "                    \"与很多人一起会\",\n" +
            "                    \"以下哪个词语更符合你\",\n" +
            "                    \"以下哪个词语更符合你\",\n" +
            "                    \"在社交聚会中，你\",\n" +
            "                    \"你通常\",\n" +
            "                    \"哪些人会更吸引你？\",\n" +
            "\"当你因为按规则办事 而触碰了朋友的些许利益时，你通常\","+
            "                    \"你认为别人一般\",//下面从对应表格的59题开始，直至78题，index从27到46\n" +
            "                ]";

    public String getAnswer() { return answer; }


    public String getOptions() { return options; }
    public void setOptions(String value) { this.options = value; }

    public String getQues() { return ques; }
    public void setQues(String value) { this.ques = value; }
}

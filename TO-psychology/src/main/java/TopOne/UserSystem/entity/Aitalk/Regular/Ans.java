package TopOne.UserSystem.entity.Aitalk.Regular;

public class Ans {
    private String mood_ans="[\n" +
            "    [\"A. 夏天\", \"B. 冬天\"],\n" +
            "    [\"A. 乐观\", \"B. 悲观\"],\n" +
            "    [\"A. 是的\", \"B. 不是的\"],\n" +
            "    [\"A. 在家里宅着\", \"B. 外出社交\"],\n" +
            "    [\"A. 积极应对\", \"B. 逃避\"],\n" +
            "    [\"A. 是的\", \"B. 不是的\"],\n" +
            "    [\"A. 接受并改进\", \"B. 回击或抵触\"],\n" +
            "    [\"A. 是的\", \"B. 不是的\"],\n" +
            "    [\"A. 寻求帮助\", \"B. 独自应对\"],\n" +
            "    [\"A. 是的\", \"B. 不是的\"],\n" +
            "    [\"A. 独立完成任务\", \"B. 团队合作\"],\n" +
            "    [\"A. 是的\", \"B. 不是的\"],\n" +
            "    [\"A. 作为学习和成长的机会\", \"B. 失望和打击\"],\n" +
            "    [\"A. 是的\", \"B. 不是的\"],\n" +
            "    [\"A. 坚持努力并寻找解决办法\", \"B. 感到沮丧并放弃\"],\n" +
            "    [\"A. 正面方面\", \"B. 负面方面\"],\n" +
            "    [\"A. 是的\", \"B. 不是的\"],\n" +
            "    [\"A. 与朋友聊天或寻求支持\", \"B. 独自思考或静心\"],\n" +
            "    [\"A. 是的\", \"B. 不是的\"]\n" +
            "  ]";

    public String getMood_Ans() {
        return mood_ans;
    }
}

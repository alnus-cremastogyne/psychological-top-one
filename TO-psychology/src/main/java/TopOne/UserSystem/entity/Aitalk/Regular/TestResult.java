package TopOne.UserSystem.entity.Aitalk.Regular;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_testresult")
public class TestResult {
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    private String type;
    private String level;
    private String result;
    private String text;

}

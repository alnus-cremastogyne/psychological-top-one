package TopOne.UserSystem.entity.Aitalk.MBTI;

/**
 * @author suze
 * @date 2023/11/6
 * @time 11:59
 **/
public class Answer {
    private long id;
    private String type;
    /**
     * 这里只能是真或假
     */
    private boolean btype;

    /**
     * 这里的ESTJ其中一个
     */
    private boolean bool;

    public boolean getBool() {

        return bool; }
    public void setBool(boolean value) { this.bool = value; }

    public boolean getBtype() { return btype; }
    public void setBtype(boolean value) { this.btype = value; }

    public long getid() { return id; }
    public void setid(long value) { this.id = value; }

    public String getType() { return type; }
    public void setType(String value) { this.type = value; }
}

package TopOne.UserSystem.entity.Aitalk;
import cn.hutool.core.util.RandomUtil;

import java.util.ArrayList;
import java.util.List;

public class Recommendation {
    private List<String> questions = new ArrayList<>();
    private List<String> newQues = new ArrayList<>();


    public List<String> getNewQues() {
        for (int i = 0; i < 2; i++) {
            int index = RandomUtil.randomInt(0,questions.size());
            newQues.add(questions.get(index));
        }
        return newQues;
    }

    public Recommendation() {
        questions.add("压力管理");
        questions.add("自我探索");
        questions.add("人际关系");
        questions.add("恐惧症状");
        questions.add("情感失落");
        questions.add("疾病焦虑");
        questions.add("家庭冲突");
        questions.add("性格障碍");
        questions.add("失眠困扰");
        questions.add("心理成长");
        questions.add("学业压力");
        questions.add("职场挑战");
        questions.add("社交焦虑");
        questions.add("抑郁情绪");
        questions.add("自卑感");
        questions.add("失去动力");
        questions.add("恋爱困惑");
        questions.add("人际冲突");
        questions.add("心理幸福");
        questions.add("家庭失和");
        questions.add("自我认知");
        questions.add("厌食症状");
        questions.add("自我价值");
        questions.add("亲密关系");
        questions.add("身体形象");
        questions.add("心理创伤");
        questions.add("适应困难");
        questions.add("自我放松");
        questions.add("亲子关系");
        questions.add("焦虑症状");
        questions.add("社交压力");
        questions.add("学习困扰");
        questions.add("职业发展");
        questions.add("自我接纳");
        questions.add("情绪调节");
        questions.add("工作压力");
        questions.add("自信心");
        questions.add("婚姻问题");
        questions.add("心理辅导");
        questions.add("恢复自信");
        questions.add("社交技巧");
        questions.add("青春期问题");
        questions.add("心理健康");
        questions.add("离婚困扰");
        questions.add("焦虑症治疗");
        questions.add("心理咨询");
        questions.add("失去爱意");
        questions.add("性心理问题");
        questions.add("人际沟通");
        questions.add("心理疲劳");
        questions.add("决策困难");
        questions.add("人际关怀");
        questions.add("心理平衡");
        questions.add("恢复自我");
        questions.add("社交技能");
        questions.add("青少年问题");
        questions.add("心理治疗");
        questions.add("工作满意度");
        questions.add("婚姻挽回");
        questions.add("性功能障碍");
        questions.add("人际关系建立");
        questions.add("心理调适");
        questions.add("情绪管理");
        questions.add("职场人际关系");
    }
}

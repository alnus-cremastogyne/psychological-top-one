package TopOne.UserSystem.entity.Listener;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

/***
 * @title Appointment
 * @author SUZE
 * @Date 17:27
 **/
@Data
@EqualsAndHashCode(callSuper = false)//不考虑父类字段
@Accessors(chain = true)//允许链式编程 setter和getter方法
@TableName("tb_new_appointment")
public class Appointment {
    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private int id;
    private String name; // 姓名
    private String gender; // 性别
    private String birthday; // 生日
    private String major; // 专业
    private String department; // 系部
    private String campusAddress; // 校内地址
    private String phoneNumber; // 电话号码
    private String appointmentType; // 预约类型
    private String reasonForVisit; // 来访原因
    private String severity; // 症状严重程度
    private String symptomOnset; // 症状发生时间
    private String selfEvaluation; // 自我评估
    private String helpNeeded; // 需要什么帮助
    private Date appointmentDate; // 预约日期

    @Value("user_id")
    private String userId; // 用户ID
    @Value("listener_id")
    private int  listenerId; // 监听者ID
    @Value("appointment_time")
    private String appointmentTime; // 预约时间
}

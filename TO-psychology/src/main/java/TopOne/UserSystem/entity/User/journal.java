
package TopOne.UserSystem.entity.User;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = false)//不考虑父类字段
@Accessors(chain = true)//允许链式编程 setter和getter方法
@TableName("tb_journal")
public class journal implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String userId;

    private short valueEmotion;

    private String writeContent;

    private String createdAt;

}

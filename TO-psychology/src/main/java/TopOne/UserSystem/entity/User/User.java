package TopOne.UserSystem.entity.User;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 /**
 *@author suze
 *@date 2023-10-25
 *@time 15:20
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_user")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 手机号码
     */
    private String phone;
    /**
     * openId和sessionKey
     *
     */

    private String openId;

    private String sessionKey;
    /**
     * 昵称，默认是随机字符
     */
    private String name;
    private String moodword;

    /**
     * 用户头像
     */
    private String icon;
    private String collection;
    private int age;

    /**
     *心理描述 后期再做加密
     **/
    private  String psychologicalDescription;

    /**
     * 创建时间
     */
    private LocalDateTime createdAt;

    private String lec;

    /**
     * 更新时间
     */
    private LocalDateTime updatedAt;


}

package TopOne.UserSystem.entity.index;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_book")
public class Book {

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private long id;
    /**
     * 作者
     */
    private String author;
    /**
     * 正文
     */
    private String body;
    private String chineseQuote;
    private String englishQuote;

    private String imgUrl;
    private LocalDate publishDate;
    /**
     * 发布平台
     */
    private String publishingPlatform;
    /**
     * 阅读人数 默认为0
     */
    private long readCount;
    /**
     * 标题
     */
    private String title;

}

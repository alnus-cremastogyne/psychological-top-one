
package TopOne.UserSystem.entity.index.Lec;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_lec")
public class Lec {

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private long id;
    /**
     * 允许报名
     */
    private boolean allowListen;
    /**
     * 正文
     */
    private String body;
    private String imgUrl;

    private int listenerNumber;
    /**
     * 地点
     */
    private String publishingPlatform;
    /**
     * 日期
     */
    private String releaseDate;
    /**
     * 讲师
     */
    private String teacher;
    /**
     * 标题
     */
    private String title;
    private String stu;
    private String lecPurpose;
    private String scope;
    private String isLecEssay;

    public boolean isAllowListen() {
        return allowListen;
    }
}

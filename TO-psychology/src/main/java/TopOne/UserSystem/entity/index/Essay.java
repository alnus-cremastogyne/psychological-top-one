package TopOne.UserSystem.entity.index;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_essay2")
public class Essay {

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 中文名言
     */
    @TableField("chinese_quote")
    private String chinese_quote;

    /**
     * 英文名言
     */
    private String english_quote;
    /**
     * 发布日期名言
     */
    private String publish_date;
    /**
     * 作者
     */
    private String author;
    /**
     * 正文
     */
    private String body;
    private List<String> img_url;
    /**
     * 发布平台
     */
    private String publishing_platform;
    /**
     * 发布日期
     */
    private String release_date;
    /**
     * 标题
     */
    private String title;
}

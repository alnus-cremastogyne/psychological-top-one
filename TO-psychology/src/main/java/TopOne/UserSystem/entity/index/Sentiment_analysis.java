package TopOne.UserSystem.entity.index;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_sentiment_analysis")
public class Sentiment_analysis {

    /**
     * 主键
     */
    @TableId(value = "id")
    private String id;
    private String date;
    private long mood;
    private long pressure;
    private long sentiment;
}


package TopOne.UserSystem.entity.index.Lec;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_lec_essay")
public class LecEssay {

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private long id;
    private String body;
    private String introduce;
    private String title;
    private String  url;

}



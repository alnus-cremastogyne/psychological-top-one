
package TopOne.UserSystem.entity.index.Lec;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_lec_history")
public class LecHistory {

    @TableId(value = "id", type = IdType.AUTO)
    private long id;
    private long lec_id;
    private String user_id;

    public long getLec_id() { return lec_id; }
    public void setLec_id(long value) { this.lec_id = value; }

    public String getUser_id() { return user_id; }
    public void setUser_id(String value) { this.user_id = value; }
}

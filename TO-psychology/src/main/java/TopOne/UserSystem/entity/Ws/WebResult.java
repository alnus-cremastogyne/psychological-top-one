package TopOne.UserSystem.entity.Ws;

public class WebResult {

    private  Object message;
    private boolean System;
    private  String formName;

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public boolean isSystem() {
        return System;
    }

    public void setSystem(boolean system) {
        System = system;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }
}

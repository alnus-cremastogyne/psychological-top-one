package TopOne.UserSystem.mapper;

import TopOne.UserSystem.entity.Listener.Appointment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/***
 * @title Appointment2Mapper
 * @author SUZE
 * @Date 17:23
 **/
@Mapper
public interface Appointment2Mapper extends BaseMapper<Appointment> {
}

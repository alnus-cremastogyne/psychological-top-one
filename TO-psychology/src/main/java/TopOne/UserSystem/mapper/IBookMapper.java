package TopOne.UserSystem.mapper;

import TopOne.UserSystem.entity.index.Book;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface IBookMapper extends BaseMapper<Book> {
}

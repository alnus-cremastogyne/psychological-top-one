package TopOne.UserSystem.mapper;

import TopOne.UserSystem.entity.index.Sentiment_analysis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface Sentiment_analysisMapper extends BaseMapper<Sentiment_analysis> {
}


package TopOne.UserSystem.mapper;

import TopOne.UserSystem.entity.index.Lec.LecHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface LecHistoryMapper extends BaseMapper<LecHistory> {
}

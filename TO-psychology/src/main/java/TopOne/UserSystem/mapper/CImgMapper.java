package TopOne.UserSystem.mapper;

import TopOne.UserSystem.entity.index.Carousel.CarouselImg;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author suze
 * @date 2023/11/10
 * @time 12:50
 **/
public interface CImgMapper extends BaseMapper<CarouselImg> {

}

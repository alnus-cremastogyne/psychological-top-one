package TopOne.UserSystem.mapper;

import TopOne.UserSystem.entity.User.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 /**
 *@author suze
 *@date 2023-10-25
 *@time 15:21
 **/
public interface UserMapper extends BaseMapper<User> {

}

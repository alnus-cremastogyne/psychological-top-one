package TopOne.UserSystem.mapper;

import TopOne.UserSystem.entity.Aitalk.Regular.TestResult;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface TestResultMapper extends BaseMapper<TestResult> {
}

package TopOne.UserSystem.mapper;

import TopOne.UserSystem.entity.index.Questionnaire;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface QuestionMapper extends BaseMapper<Questionnaire> {

}

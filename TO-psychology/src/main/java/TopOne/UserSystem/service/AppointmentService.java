
package TopOne.UserSystem.service;

import TopOne.ListenerSystem.entity.Listener;
import TopOne.UserSystem.entity.Listener.Appointment;
import TopOne.dto.Result;

import java.util.List;

public interface AppointmentService {

    Result uploadAppointment(Appointment appointment);

    Result deleteAppointment(int ListenerId, String userId);

    List<Appointment> getByListener(int listenId,String time);

    Appointment queryByOpenId(String open_id);

    Listener getListenerByUserId(String userId);

//    Object GetV(String listenerId);
}

package TopOne.UserSystem.service;

import TopOne.dto.Result;
import TopOne.UserSystem.entity.index.Book;
import com.baomidou.mybatisplus.extension.service.IService;

public interface BookService extends IService<Book> {

    Result queryList();

    Book queryById(int id);
}

package TopOne.UserSystem.service.impl;

import TopOne.dto.Result;

import TopOne.UserSystem.entity.index.Sentiment_analysis;
import TopOne.UserSystem.mapper.Sentiment_analysisMapper;
import TopOne.UserSystem.service.Sentiment_analysisService;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

@Service
@Slf4j
@Primary
public class Sentiment_analysisServiceImpl extends ServiceImpl<Sentiment_analysisMapper, Sentiment_analysis> implements Sentiment_analysisService {

    @Resource
    StringRedisTemplate stringRedisTemplate;
    @Override
    public Result comeOut(String userId, JSONArray answers) {
        int moodScore = 0;
        int sentimentScore = 0;
        int pressureScore = 0;
        System.out.println(answers);
        for (Object answer : answers) {
            cn.hutool.json.JSONObject jsonObject = new cn.hutool.json.JSONObject(answer);
            switch (jsonObject.getStr("type")){
                case "mood":
                    moodScore += jsonObject.getInt("value");
                    break;
                case "sentiment":
                    sentimentScore+=jsonObject.getInt("value");
                    break;
                case "pressure":
                    pressureScore+=jsonObject.getInt("value");
                    break;
            }
        }
        System.out.println("111");
        // 根据答题结果进行情绪、心情、压力等的计算和判断
        Sentiment_analysis analysis = this.getById(userId);
        if (analysis==null){
            //第一次测  创建用户
            analysis = createAnalysis(userId);
        }

        //记录当日的日期放进Sentiment_analysis当中
            analysis.setDate(new Date().toString());
            analysis.setMood(moodScore);
            analysis.setSentiment(sentimentScore);
            analysis.setPressure(pressureScore);
        // 进行相应的逻辑处理，生成曲线图数据
        return Result.ok(analysis);
    }

    @Override
    public JSONArray GetByid(String id) {
        //这里还没做测试的具体操作 先做样板数
        JSONArray answers = new JSONArray();
        for (int i=0;i<6;i++){
            answers.add(RandomUtil.randomInt(6,9));
        }
        return answers;
    }

    private Sentiment_analysis createAnalysis(String userId) {
        Sentiment_analysis analysis = new Sentiment_analysis();
        analysis.setId(userId);
        analysis.setMood(0);
        analysis.setSentiment(0);
        analysis.setPressure(0);
        analysis.setDate(new Date().toString());

        this.save(analysis);
        return analysis;
    }

    }




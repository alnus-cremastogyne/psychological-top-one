package TopOne.UserSystem.service.impl;

import TopOne.UserSystem.entity.User.UserBookLec;
import TopOne.UserSystem.entity.index.Book;
import TopOne.UserSystem.service.BookService;
import TopOne.UserSystem.service.LecService;
import TopOne.UserSystem.service.UserBookLecService;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONUtil;
import cn.hutool.json.JSONArray;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import TopOne.dto.Result;
import TopOne.UserSystem.entity.User.User;
import TopOne.UserSystem.mapper.UserMapper;
import TopOne.UserSystem.service.IUserService;
import TopOne.utils.RegexUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static TopOne.utils.RedisConstants.*;

/**
 * <p>
 * 服务实现类
 * </p>
 /**
 *@author suze
 *@date 2023-10-25
 *@time 15:22
 **/
@Service
@Slf4j
@Primary
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Resource
    private BookService bookService;
    @Resource
    private LecService lecService;
    @Resource
    private UserBookLecService userBookLecService;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Override
    public Result setIcon(String url, String id) {
        User user = query().eq("open_id",id).one();
        if (user==null){
            return Result.fail("用户不存在");
        }
        user.setIcon(url);
        update(user,new UpdateWrapper<User>().eq("open_id",id));
        return Result.ok(url);
    }
    @Override
    public User upDataMe(User user, String token, String userId) {
        if (!update(user,new UpdateWrapper<User>().eq("open_id",userId))) {
            return null;
        }
        //改redis
        stringRedisTemplate.opsForValue().set(LOGIN_USER_KEY+token,JSONUtil.toJsonStr(user),3,TimeUnit.HOURS);
        return user;
    }

    @Override
    public String writeWord(String word, String id) {
        User user = query().eq("open_id",id).one();
        if (user==null){
            return "用户不存在";
        }
        user.setMoodword(word);
        update(user,new UpdateWrapper<User>().eq("open_id",id));
        return word;
    }
    @Override
    public String getWord(String id) {
        User user = query().eq("open_id",id).one();
        if (user==null){
            return "用户不存在";
        }
        return user.getMoodword();
    }

    @Override
    public String login(String OpenId, String name, String icon, String sessionKey, String Token) {
        //查redis
        String jsonstr = stringRedisTemplate.opsForValue().get(LOGIN_USER_KEY+Token);
        if (jsonstr!=null)
        //在redis中存在 直接返回token 并延长有效期
        {
            stringRedisTemplate.expire(Token, 3, TimeUnit.HOURS);
            //User user = JSON.parseObject(jsonstr, User.class);
            return Token;
        }
        //如果在缓存中找不到token了说明登录过期了  定义自己的token
        String token=OpenId+RandomUtil.randomString(10);
        //一致，检查open id是否在数据库中
        User user = query().eq("open_id", OpenId).one();
        //不存在，创建新用户
        if(user==null){
            user= createUser(OpenId, name, icon,sessionKey);
            System.err.println("新用户");
        }
        //将用户保存到Redis
        //将转换好的user放进hashRedis里 设置有效期 登陆校验器记得加上 LOGIN_USER_KEY！
        stringRedisTemplate.opsForValue().set(LOGIN_USER_KEY+token, JSONUtil.toJsonStr(user),3,TimeUnit.HOURS);
        System.err.println("已登录，token是"+token);
        return  token;
    }


    @Override        //这里重新写两个收藏表 文章和讲座 关联用户id 收藏文章的id 讲座的id
    public Result collection(String userId, int Id, String type) {
        //userId若为空直接返回未登录
        if (userId==null){
            return Result.fail("未登录");

        }
        if (userBookLecService.collection(userId,Id,type)){
            return Result.ok("收藏成功");
        }
        return Result.ok("取消收藏成功");
    }

    @Override
    public List GetCollection(String userId, String type) {
        List<UserBookLec> list = userBookLecService.GetCollection(userId, type);
        if (list==null){
            return null;
        }
        List collection=new ArrayList();
        if (type.equals("book")){
            //文章就在文章表里面获取文章 根据list里面每一个元素的id
            for (UserBookLec userBookLec : list) {
                //如果userId对得上
                if (userId.equals(userBookLec.getUserId())) {
                    //根据收藏表获取文章id
                    //用bookService去根据文章id查
                    collection.add(bookService.getById(userBookLec.getId()));
                }
            }
        }
        else if (type.equals("lec")){
            //讲座就在讲座表里面获取讲座 根据list里面每一个元素的id
            for (UserBookLec userBookLec : list) {
                //如果userId对得上
                if (userId.equals(userBookLec.getUserId())) {
                    //根据收藏表获取讲座id
                    //用bookService去根据讲座id查
                    collection.add(lecService.getById(userBookLec.getId()));
                }
            }
        }
        return collection;
    }


    @Override
    public User queryByid(String userid) {

        String userjson = stringRedisTemplate.opsForValue().get("user:" + userid);
        if (userjson!=null){
            User user = JSONUtil.toBean(userjson, User.class);
            stringRedisTemplate.expire("user:" + userid, 2, TimeUnit.DAYS);
            return user;
        }
        User user = query().eq("open_id", userid).one();
        stringRedisTemplate.opsForValue().set("user:"+userid,JSONUtil.toJsonStr(user),1,TimeUnit.DAYS);
        return user;

    }




    private static boolean isDuplicate(JSONArray jsonArray, int element) {
        for (Object obj : jsonArray) {
            if (obj instanceof Integer && ((Integer) obj).intValue() == element) {
                return true;
            }
        }
        return false;
    }

    //这里的User ServiceImpl继承了ServiceImpl，所以可以调用父类的方法
    //所以这个类是知道要操作的数据库是哪个（UserMapper）

    @Override
    public Result sendCode(String phone, HttpSession session) {
        //效验手机号
        if(RegexUtils.isPhoneInvalid(phone)){
            log.debug(phone);
            log.debug("手机号格式错误");
            return Result.fail("手机号格式错误");
        }
        //生成验证码
        String code =RandomUtil.randomNumbers(6);
        //保存验证码到redis
        stringRedisTemplate.opsForValue().set(LOGIN_CODE_KEY+phone,code,LOGIN_CODE_TTL, TimeUnit.MINUTES);
        //发送验证码
        log.debug("验证码为："+code);
        return Result.ok();
    }



    @Override
    public Result setPsychological(String description, String token) {

        //拿着他的token 去redis查登陆态
        String str = stringRedisTemplate.opsForValue().get(token);
        //没查到就是没登陆 返回错误请重新登陆！
        if (str==null){
            return Result.fail("请重新登陆！");
        }
        //查到了 就是登陆了
        //TODO 密文加密  得到密文 和密钥
        String encryptstr = encrypt(description);

        // 然后根据token查到user的json字符  然后转为user对象
        User user = JSONUtil.toBean(stringRedisTemplate.opsForValue().get(token), User.class);
        //设置心理描述（密文和密钥包装成json字符一起存到数据库）
        user.setPsychologicalDescription(encryptstr);
        return  Result.ok("描述加密设置成功");
    }

    @Override
    public Result logout(String token) {
        stringRedisTemplate.opsForValue().decrement(LOGIN_USER_KEY+token);
        return Result.ok("退出成功");
    }



    //区块链的加密函数 返回密文和密钥包装成json字符
    public String encrypt(String description) {
        //密文和密钥包装成json字符
        String json=null;
        //TODO 加密


        return json;
    }



    private User createUser(String OpenId, String name, String icon,String sessionKey) {
        //创建用户
        User user = new User();
        user.setOpenId(OpenId);
        if (name == null){
            user.setName("user_"+RandomUtil.randomNumbers(6));
        }
        user.setName(name);
        user.setIcon(icon);
        user.setAge(18);
        user.setPhone("该用户未绑定手机号");
        user.setSessionKey(sessionKey);
        user.setPsychologicalDescription("这个人很懒，什么都没有留下");
        //保存用户
        save(user);
        return user;

    }
}

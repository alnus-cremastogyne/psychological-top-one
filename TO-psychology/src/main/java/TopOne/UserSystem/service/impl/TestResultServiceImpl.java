package TopOne.UserSystem.service.impl;

import TopOne.UserSystem.entity.Aitalk.Regular.TestResult;
import TopOne.UserSystem.mapper.TestResultMapper;
import TopOne.UserSystem.service.TestResultService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@Primary
public class TestResultServiceImpl extends ServiceImpl<TestResultMapper,TestResult> implements TestResultService
{

    @Override
    public TestResult getresult(int score, String type) {
        String level;
        switch (score) {
            case 0:
                level = "E";
                break;
            case 1:
            case 2:
                level = "D";
                break;
            case 3:
            case 4:
            case 5:
            case 6:
                level = "C";
                break;
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                level = "B";
                break;
            default:
                level = "A";
                break;
        }
        //创建一个mapper用来存查询条件
        QueryWrapper<TestResult> queryWrapper = new QueryWrapper<>();
        //根据等级和类型查询
        // 这里type只有五大类型mood interpersonal Love growth career
        queryWrapper.eq("level", level).eq("type", type);
        TestResult testResult = getOne(queryWrapper);

        if (testResult != null) {
            //testResult.setText(testResult.getText().replace("\\n",""));

            return testResult;
        } else {
            return null;
        }
    }
}

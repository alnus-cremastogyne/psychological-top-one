
package TopOne.UserSystem.service.impl;

import TopOne.dto.Result;
import TopOne.UserSystem.entity.User.TestHistory;
import TopOne.UserSystem.mapper.TestHistoryMapper;
import TopOne.UserSystem.service.TestHistoryService;
import cn.hutool.json.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
@Slf4j
@Primary
public class TestHistoryServiceImpl extends ServiceImpl<TestHistoryMapper, TestHistory> implements TestHistoryService
{

    @Override
    public Object putMbti(String id, String data)
    {
        TestHistory testHistory = new TestHistory();
            testHistory.setOpenId(id);
            testHistory.setMbti(data);
            save(testHistory);
        return null;
    }

    @Override
    public Result getHistory(String userId) {

        List<TestHistory> testHistoryList = query().eq("open_id",userId).list();
        JSONArray data=new JSONArray();
        System.out.println(testHistoryList);
        int i=0;

        for (TestHistory testHistory:
             testHistoryList) {
            System.out.println(testHistory);
            JSONObject jsonObject = new JSONObject();
            //下面的每一个历史记录加上一个标题
            if (testHistory.getMbti() != null) {
                JSONObject MBTI = JSONObject.parseObject(testHistory.getMbti().replace("\\", ""));
                MBTI.put("title", "MBTI人格测试");
                jsonObject.put("MBTI", MBTI);
            }

            if (testHistory.getRegularTestMood() != null) {
                JSONObject moodObject = JSONObject.parseObject(testHistory.getRegularTestMood().replace("\\", ""));
                moodObject.put("title", "情绪管理心理健康测试");
                jsonObject.put("mood", moodObject);
            }

            if (testHistory.getRegularTestInterpersonal() != null) {
                JSONObject interpersonalObject = JSONObject.parseObject(testHistory.getRegularTestInterpersonal().replace("\\", ""));
                interpersonalObject.put("title", "人际交往心理健康测试");
                jsonObject.put("interpersonal", interpersonalObject);
            }

            if (testHistory.getRegularTestLove() != null) {
                JSONObject loveObject = JSONObject.parseObject(testHistory.getRegularTestLove().replace("\\", ""));
                loveObject.put("title", "恋爱建议心理健康测试");
                jsonObject.put("love", loveObject);
            }

            if (testHistory.getRegularTestGrowth() != null) {
                JSONObject growthObject = JSONObject.parseObject(testHistory.getRegularTestGrowth().replace("\\", ""));
                growthObject.put("title", "成长建议心理健康测试");
                jsonObject.put("growth", growthObject);
            }

            if (testHistory.getRegularTestCareer() != null) {
                JSONObject careerObject = JSONObject.parseObject(testHistory.getRegularTestCareer().replace("\\", ""));
                careerObject.put("title", "职业规划心理健康测试");
                jsonObject.put("career", careerObject);
            }

            data.add(jsonObject);
            i++;
        }
        return Result.ok(data);
    }

    @Override
    public Result putRegularQue(String userId, String type, String data) {
        List<TestHistory> testHistoryList = query().eq("open_id", userId).list();
        for (TestHistory testHistory:
                testHistoryList) {
            if (testHistory!=null){
                if (type.equals("mood")) {
                    testHistory.setRegularTestMood(data);
                } else if (type.equals("interpersonal")) {
                    testHistory.setRegularTestInterpersonal(data);
                } else if (type.equals("love")) {
                    testHistory.setRegularTestLove(data);
                } else if (type.equals("growth")) {
                    testHistory.setRegularTestGrowth(data);
                } else {
                    testHistory.setRegularTestCareer(data);
                }
                updateById(testHistory);
            }
            else {
                testHistory = new TestHistory();
                testHistory.setOpenId(userId);
                if (type.equals("mood")) {
                    testHistory.setRegularTestMood(data);
                } else if (type.equals("interpersonal")) {
                    testHistory.setRegularTestInterpersonal(data);
                } else if (type.equals("love")) {
                    testHistory.setRegularTestLove(data);
                } else if (type.equals("growth")) {
                    testHistory.setRegularTestGrowth(data);
                } else {
                    testHistory.setRegularTestCareer(data);
                }
                save(testHistory);
            }
            System.out.println(testHistory);
        }
        return Result.ok();
    }


}

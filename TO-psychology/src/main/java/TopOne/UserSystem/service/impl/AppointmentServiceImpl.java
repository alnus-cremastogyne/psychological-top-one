package TopOne.UserSystem.service.impl;


import TopOne.ListenerSystem.entity.Listener;
import TopOne.ListenerSystem.service.ListenerService;
import TopOne.UserSystem.entity.Listener.Appointment;
import TopOne.UserSystem.mapper.Appointment2Mapper;
import TopOne.dto.Result;
import TopOne.UserSystem.mapper.AppointmentMapper;
import TopOne.UserSystem.service.AppointmentService;
import TopOne.utils.RedisTable.ReservationService;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Options;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
@Primary
public class AppointmentServiceImpl extends ServiceImpl<Appointment2Mapper, Appointment> implements AppointmentService {
    @Resource
    private ReservationService reservationService;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private ListenerService listenerService;
    @Override
    public Result uploadAppointment(Appointment appointment) {
        save(appointment);
        System.out.println(appointment);
        return Result.ok("用户预约表上传成功");
    }

    @Autowired
    private AppointmentMapper appointmentMapper;
    @Override
    public Result deleteAppointment(int ListenerId, String userId) {
//        QueryWrapper<Appointment> queryWrapper = new QueryWrapper<>();
//        queryWrapper.eq("listener_id", Integer.toString(ListenerId));
//        queryWrapper.eq("user_id", userId);
//        int deletedRows = appointmentMapper.delete(queryWrapper);
//        Appointment appointment = this.baseMapper.selectOne(queryWrapper);
        Appointment appointment = queryByOpenId(userId);
        //把缓存给删了
        stringRedisTemplate.delete("Talking:"+userId);
        System.out.println("删除预约记录的缓存成功！");
        if (appointment == null) {
            return Result.fail("没有预约！");
        }
        Date date = appointment.getAppointmentDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String DateStr = sdf.format(date);
        String time = appointment.getAppointmentTime();
        reservationService.UnReserved(Integer.toString(ListenerId),DateStr, time);
        //System.out.println("WWWWWWWWWWWW:"+appointment.getId());
        int deletedRows = appointmentMapper.deleteById(appointment.getId());
        if (deletedRows > 0) {
            return Result.ok("删除成功");
        } else {
            return Result.ok("删除失败");
        }
    }

    @Override
    @Options(useCache = false)
    public List<Appointment> getByListener(int listenId, String time) {
        QueryWrapper queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("listener_id",listenId);
        List<Appointment> listAll = this.baseMapper.selectList(queryWrapper);
        List<Appointment> list = new ArrayList<>();
        if (listAll==null){
            return null;
        }
        for (Appointment appointment : listAll) {
            System.out.println(appointment);
            //这里记得和前端说这个预约时间的格式 要是json格式week专门分一个对象出来  其他就全赛一个对象里面
            String thisTime = appointment.getAppointmentTime();
            if (thisTime.equals(time)) {
                list.add(appointment);
            }
        }
        return listAll;
    }

    @Override
    public Appointment queryByOpenId(String open_id) {
        Appointment one = query().eq("user_id", open_id).one();
        System.out.println("预约表单："+one);
                return one;
    }

    @Override
    public Listener getListenerByUserId(String userId) {
        String Key="Talking:"+userId;//这个缓存了 用户跟谁聊天 的信息
        String json = stringRedisTemplate.opsForValue().get(Key);
        Listener listener=null;
        if (json==null){
            //缓存未命中 重新查询
            Appointment appointment = queryByOpenId(userId);
            if (appointment==null){
                //这里会有用户没有预约表单的情况
                System.out.println("用户没有预约表单");
                return  null;
            }
            int listenerId = appointment.getListenerId();
            listener = listenerService.getById(listenerId);
            System.out.println("dayin"+listener);
        }
        else {
            System.out.println("缓存得到了跟谁聊天的信息");
            listener=JSONUtil.toBean(json,Listener.class);
        }
        if (listener!=null){
            if (listener.getFeatureTags()!=null){
                listener.setFeatureTagArray(JSONUtil.parseArray(listener.getFeatureTags()));
            }
            stringRedisTemplate.opsForValue().set(Key,JSONUtil.toJsonStr(listener),2, TimeUnit.HOURS);
            return listener;
        }
        System.out.println("listener不存在的情况！");
        return null;
    }

//    @Override
//    public Object GetV(String listenerId) {
//        Appointment a = this.baseMapper.selectById(listenerId);
//        System.out.println(a);
//        return a;
//    }
}

package TopOne.UserSystem.service.impl;

import TopOne.UserSystem.service.IUserService;
import TopOne.dto.Result;
import TopOne.UserSystem.entity.index.Book;
import TopOne.UserSystem.mapper.IBookMapper;
import TopOne.UserSystem.service.BookService;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
@Primary
public class BookServiceImpl extends ServiceImpl<IBookMapper, Book> implements BookService {

    @Resource
    StringRedisTemplate stringRedisTemplate;

    @Override
    public Result queryList() {
        //TODO 这里家多一个用户id 检查他是否有收藏列表当中的所有文章 在返回里面多加一个字段进去

        String bookListstr = stringRedisTemplate.opsForValue().get("BookList");
        if (bookListstr!=null){
            List<Book> bookList = JSON.parseArray(JSONUtil.toJsonStr(bookListstr), Book.class);
            return Result.ok(bookList);
        }
        List<Book> bookList = query().orderByAsc("id").list();
        //循环打印bookList里面每一个元素 的每一个 属性
        for (Book book : bookList) {
            log.info(book.toString());
        }
        stringRedisTemplate.opsForValue().set("BookList", JSONUtil.toJsonStr(bookList),1,TimeUnit.DAYS);
        return Result.ok(bookList);
    }
    @Resource
    private IUserService userService;
    @Override
    public Book queryById(int id) {
        String str = stringRedisTemplate.opsForValue().get("book:" + id);
        Book book =null;
        if (str!=null){
            book = JSON.parseObject(JSONUtil.toJsonStr(str), Book.class);
            //System.out.println("从Redis得到");
        }
        else {
            book = getById(id);
            if (book==null){
                return null;
            }
            //System.out.println("从sql得到");
            stringRedisTemplate.opsForValue().set("book:"+id,JSONUtil.toJsonStr(book),1, TimeUnit.DAYS);
        }
        return book;
    }
}

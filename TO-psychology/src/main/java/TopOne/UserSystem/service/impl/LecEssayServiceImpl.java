
package TopOne.UserSystem.service.impl;

import TopOne.UserSystem.entity.index.Lec.LecEssay;
import TopOne.UserSystem.mapper.LecEssayMapper;
import TopOne.UserSystem.service.LecEssayService;
 import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
@Primary
public class LecEssayServiceImpl extends ServiceImpl<LecEssayMapper, LecEssay> implements LecEssayService {
    @Resource
    StringRedisTemplate stringRedisTemplate;
    @Override
    public LecEssay queryLecEssayById(Integer id) {
        String jsonstr = stringRedisTemplate.opsForValue().get("LEssay:" + id);
        if (jsonstr!=null){
            LecEssay lecEssay = JSON.parseObject(jsonstr, LecEssay.class);
            return lecEssay;
        }
        LecEssay lecEssay = getById(id);
        if(lecEssay==null){
            return null;
        }
        stringRedisTemplate.opsForValue().set("LEssay:" + id, JSONUtil.toJsonStr(lecEssay),1, TimeUnit.DAYS);
        return lecEssay;
    }

}



package TopOne.UserSystem.service.impl;

import TopOne.dto.Result;
import TopOne.UserSystem.entity.index.Lec.LecHistory;
import TopOne.UserSystem.mapper.LecHistoryMapper;
import TopOne.UserSystem.service.LecHistoryService;
 import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@Primary
public class LecHistoryServiceImpl extends ServiceImpl<LecHistoryMapper, LecHistory> implements LecHistoryService
{

    //这里要定时清理这个预约讲座的表 因为现在还没有写清除预约记录
    // 这个方法要作为老师的后台 结束该讲座就自动清理所有讲座相关的预约记录

    @Override
    public void putHistory(int id, String userId) {
        LecHistory lecHistory = new LecHistory();
        lecHistory.setLec_id(id);
        lecHistory.setUser_id(userId);
        save(lecHistory);
    }

    @Override
    public Result isBooking(int id, String userId) {
        QueryWrapper<LecHistory> queryWrapper =new QueryWrapper<>();
        queryWrapper.eq("lec_id",id).eq("user_id",userId);
        if (count(queryWrapper)<=0){
            return Result.ok("false");
        }
        return Result.ok("true");

    }
}


package TopOne.UserSystem.service.impl;

import TopOne.UserSystem.entity.Aitalk.MBTI.MbtiResult;
import TopOne.UserSystem.mapper.MResultMapper;
import TopOne.UserSystem.service.MRService;
 import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;


@Service
@Slf4j
@Primary
public class MRServiceImpl extends ServiceImpl<MResultMapper, MbtiResult> implements MRService
{

    @Override
    public MbtiResult getByType(String type) {
        QueryWrapper<MbtiResult> queryWrapper =new QueryWrapper<>();
        queryWrapper.eq("type", type);
        MbtiResult result = getOne(queryWrapper);
        return result;
    }
}

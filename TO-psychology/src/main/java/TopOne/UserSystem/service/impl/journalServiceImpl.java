
package TopOne.UserSystem.service.impl;

import TopOne.dto.Result;
import TopOne.UserSystem.entity.User.journal;
import TopOne.UserSystem.mapper.journalMapper;
import TopOne.UserSystem.service.journalService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;


@Service
@Slf4j
@Primary
public class journalServiceImpl extends ServiceImpl<journalMapper, journal> implements journalService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

//    将日志数据通过实体类保存到数据库，并判断是否存在要修改
    @Override
    public Result uploadLog(String userid, short emotion_values, String content, String write_day) {
        QueryWrapper<journal> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", userid)
                .eq("created_at", write_day);
        journal log = getOne(queryWrapper);
        if (log == null) {
            log = new journal();
            log.setUserId(userid);
            log.setValueEmotion(emotion_values);
            log.setWriteContent(content);
            log.setCreatedAt(write_day);
            save(log);
            return Result.ok("Upload log successfully");
        }
        else{
            log.setValueEmotion(emotion_values);
            log.setWriteContent(content);
            updateById(log);
            return Result.ok("Update success");
        }
    }

//    上传用户日志数据，通过userID判断用户。问题：myBatis的缓存，未写缓存
    @Override
    public Result getList(String userid, String date) {

        List<journal> logListAll = query().eq("user_id",userid).orderByAsc("id").list();
        List<journal> logList=new ArrayList<>();
        for (journal journal : logListAll) {
            String  createdAt = journal.getCreatedAt();
            if (createdAt.equals(date)){
                return Result.ok(journal);
            }
        }

        return Result.ok("没有写日志哦");
    }
}
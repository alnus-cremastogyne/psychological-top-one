package TopOne.UserSystem.service.impl;


import TopOne.UserSystem.entity.Aitalk.RegularQue;
import TopOne.UserSystem.mapper.RegularQueMapper;
import TopOne.UserSystem.service.RegularQueService;
 import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
@Primary
public class RegularQueServiceImpl extends ServiceImpl<RegularQueMapper, RegularQue> implements RegularQueService
{
    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public List<RegularQue> getByType(String type) {
        String str = stringRedisTemplate.opsForValue().get("Regular:"+type);
        if (str!=null){
            List<RegularQue> regularQues = BeanUtil.copyToList(JSONUtil.parseArray(str), RegularQue.class);
            return regularQues;
        }
        QueryWrapper<RegularQue> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type", type)
                .orderByAsc("id");
        List<RegularQue> result = baseMapper.selectList(queryWrapper);

        //转成json格式
            String jsonString = JSONUtil.toJsonStr(result);

        if (result!=null){
            stringRedisTemplate.opsForValue().set("Regular:"+type, jsonString,1, TimeUnit.DAYS);
            return result;
        }
        return null;
    }
}

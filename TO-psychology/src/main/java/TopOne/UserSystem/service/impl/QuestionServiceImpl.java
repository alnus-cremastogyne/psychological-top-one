package TopOne.UserSystem.service.impl;


import TopOne.UserSystem.entity.index.Questionnaire;
import TopOne.UserSystem.mapper.QuestionMapper;
import TopOne.UserSystem.service.QuestionnaireService;
 import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
@Primary
public class QuestionServiceImpl extends ServiceImpl<QuestionMapper, Questionnaire> implements QuestionnaireService {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public List<Questionnaire> getList() {
        //1.在Redis查询是否存在
        String queListjson = stringRedisTemplate.opsForValue().get("queList");
        //2.存在直接返回
        if (queListjson != null) {
            List<Questionnaire> quelist = BeanUtil.copyToList(JSONUtil.parseArray(queListjson),  Questionnaire.class);
            return quelist;
        }
        //3.不存在 查询数据库
        List<Questionnaire> queList =query().orderByAsc("id").list();
        //3.1数据不存在 返回错误
        if (queList == null || queList.size() == 0) {
            return null;
        }
        //4.从数据库读取到数据  转化为字符串形式存入Redis  并返回原数据
        stringRedisTemplate.opsForValue().set("queList", JSONUtil.toJsonStr(queList),10, TimeUnit.DAYS);
        return queList;
    }
}

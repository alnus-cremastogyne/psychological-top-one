package TopOne.UserSystem.service.impl;

import TopOne.dto.Result;
import TopOne.UserSystem.entity.index.Lec.Lec;
import TopOne.UserSystem.entity.User.User;
import TopOne.UserSystem.mapper.LeceMapper;
import TopOne.UserSystem.service.IUserService;
import TopOne.UserSystem.service.LecService;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
@Primary
public class LecServiceImpl extends ServiceImpl<LeceMapper, Lec> implements LecService {
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private IUserService userService;
    @Override
    public Result querLecList() {
        String lecList = stringRedisTemplate.opsForValue().get("LecList");
        if (lecList != null) {
            List<Lec> lecs = JSON.parseArray(lecList, Lec.class);
            return Result.ok(lecs);
        }
        List<Lec> lecs = query().orderByAsc("id").list();
        stringRedisTemplate.opsForValue().set("LecList", JSON.toJSONString(lecs), 1, TimeUnit.DAYS);
        return Result.ok(lecs);
    }

    @Override
    public boolean IsEssayOver(Integer id) {
        Lec lec = getById(id);
        String isLecEssay = lec.getIsLecEssay();
        if (isLecEssay.equals("1")){
            return true;
        }
        return false;
    }

    @Override
    public Lec queryById(int id) {
        String jsonstr = stringRedisTemplate.opsForValue().get("LEc:" + id);
        if (jsonstr!=null){
            Lec lec = JSON.parseObject(jsonstr, Lec.class);
            return lec;
        }
        Lec lec = getById(id);
        if(lec==null){
            return null;
        }
        stringRedisTemplate.opsForValue().set("LEc:" + id, JSONUtil.toJsonStr(lec),1, TimeUnit.DAYS);
        return lec;
    }

    @Override
    public Result book(int LecId, String realName, String phone,String userid) {
        //预约逻辑
        // 2.根据id得到讲座判断 里面的人数是否大于0 ，时间比较releaseDate 是否过期
        Lec lec = getById(LecId);
        if (!lec.isAllowListen()){
            return Result.fail("该讲座已满");
        }
        // 3.开始预约 允许人数-1，用户信息中lec增加讲座标题 开始时间
        // 讲座中stu增加用户真名（因为要给老师看到）
        lec.setStu(lec.getStu()+","+realName);
        lec.setListenerNumber(lec.getListenerNumber()-1);
        //用户要填写他的手机号   还有lec
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("open_id", userid);
        User user = userService.getBaseMapper().selectOne(queryWrapper);
        System.out.println(user);
        user.setPhone(phone);
        user.setLec(user.getLec()+","+lec.getTitle());
        userService.updateById(user);
        if (lec.getListenerNumber()<=0){
            System.out.println("0");
            lec.setAllowListen(false);
        }
        this.updateById(lec);
        return Result.ok("预约成功！");
    }
}

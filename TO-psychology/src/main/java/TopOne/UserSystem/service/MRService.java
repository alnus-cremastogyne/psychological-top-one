
package TopOne.UserSystem.service;

import TopOne.UserSystem.entity.Aitalk.MBTI.MbtiResult;



import com.baomidou.mybatisplus.extension.service.IService;

public interface MRService extends IService<MbtiResult> {
    MbtiResult getByType(String type);
}

package TopOne.UserSystem.service;

import TopOne.UserSystem.entity.Aitalk.RegularQue;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface RegularQueService extends IService<RegularQue> {


    List<RegularQue> getByType(String type);
}

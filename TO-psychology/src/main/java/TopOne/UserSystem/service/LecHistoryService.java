
package TopOne.UserSystem.service;

import TopOne.dto.Result;
import TopOne.UserSystem.entity.index.Lec.LecHistory;
 import com.baomidou.mybatisplus.extension.service.IService;

public interface LecHistoryService extends IService<LecHistory>
{

    void putHistory(int id, String userId);

    Result isBooking(int id, String userId);
}

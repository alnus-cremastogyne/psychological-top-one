package TopOne.UserSystem.service;

import TopOne.dto.Result;
import TopOne.UserSystem.entity.User.TestHistory;
import com.baomidou.mybatisplus.extension.service.IService;

public interface TestHistoryService extends IService<TestHistory> {
    Object putMbti(String id,String data);

    Result getHistory(String userId);

    Result putRegularQue(String userId, String type,String data);
}


package TopOne.UserSystem.service;

import TopOne.UserSystem.entity.index.Lec.LecEssay;
 import com.baomidou.mybatisplus.extension.service.IService;

public interface LecEssayService extends IService<LecEssay> {

    LecEssay queryLecEssayById(Integer id);
}

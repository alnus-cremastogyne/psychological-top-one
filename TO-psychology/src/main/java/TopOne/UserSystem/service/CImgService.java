package TopOne.UserSystem.service;

import TopOne.dto.Result;
import TopOne.UserSystem.entity.index.Carousel.CarouselImg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author suze
 * @date 2023/11/10
 * @time 12:53
 **/

public interface CImgService extends IService<CarouselImg> {
    Result getImg();

    Object Info();
}

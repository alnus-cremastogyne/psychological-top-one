package TopOne.UserSystem.service;

import com.baomidou.mybatisplus.extension.service.IService;
import TopOne.dto.Result;
import TopOne.UserSystem.entity.User.User;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 /**
 *@author suze
 *@date 2023-10-25
 *@time 15:23
 **/


public interface IUserService extends IService<User> {

    Result sendCode(String phone, HttpSession session);

    String login(String OpenId, String name,String icon,String sessionKey,String Token);

    Result setPsychological(String description, String token);

    Result logout(String token);

    Result setIcon(String url,String id);


    Result collection(String userId, int Id,String  type);

    List GetCollection(String userId, String type);

    User queryByid(String userid);

    User upDataMe(User user, String token, String userId);

    String writeWord(String word, String id);

    String  getWord(String id);
}

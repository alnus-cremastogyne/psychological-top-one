package TopOne.UserSystem.service;


import TopOne.UserSystem.entity.index.Questionnaire;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface QuestionnaireService extends IService<Questionnaire> {

    List<Questionnaire> getList();
}

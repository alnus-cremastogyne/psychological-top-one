package TopOne.UserSystem.service;

import TopOne.dto.Result;
import TopOne.UserSystem.entity.User.journal;
import com.baomidou.mybatisplus.extension.service.IService;

public interface journalService extends IService<journal> {
    Result uploadLog(String userid,short emotion_values, String content, String Write_day);


    Result getList(String userId,String date);
}

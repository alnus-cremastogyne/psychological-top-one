package TopOne.UserSystem.service;

import TopOne.dto.Result;
import TopOne.UserSystem.entity.index.Sentiment_analysis;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.service.IService;

public interface Sentiment_analysisService extends IService<Sentiment_analysis> {
    Result comeOut(String userId, JSONArray answers);

    JSONArray GetByid(String id);
}

package TopOne.UserSystem.service;

import TopOne.dto.Result;
import TopOne.UserSystem.entity.index.Lec.Lec;
import com.baomidou.mybatisplus.extension.service.IService;

public interface LecService extends IService<Lec> {
    Result querLecList();


    boolean IsEssayOver(Integer id);

    Lec queryById(int id);

    Result book(int LecId, String realName, String phone,String userid);
}

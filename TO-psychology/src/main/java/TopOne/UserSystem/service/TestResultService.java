package TopOne.UserSystem.service;

import TopOne.UserSystem.entity.Aitalk.Regular.TestResult;
import com.baomidou.mybatisplus.extension.service.IService;

public interface TestResultService extends IService<TestResult>
{

    TestResult getresult(int score, String type);
}

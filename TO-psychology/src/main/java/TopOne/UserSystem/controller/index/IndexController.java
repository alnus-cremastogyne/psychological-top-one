package TopOne.UserSystem.controller.index;

import TopOne.UserSystem.entity.index.Book;
import TopOne.UserSystem.service.*;
import TopOne.dto.Result;
import TopOne.UserSystem.entity.index.Lec.Lec;
import TopOne.UserSystem.entity.index.Lec.LecEssay;
import TopOne.UserSystem.service.impl.LecEssayServiceImpl;
import TopOne.utils.CacheClearer;
import TopOne.utils.StrtoJSONUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Options;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author suze
 * @date 2023/11/10
 * @time 12:57
 * 用于加载首页的初始数据
 **/
@CrossOrigin
@RequestMapping("/index")
@RestController

public class IndexController {
    @Resource
    private CImgService cImgService;
    @Resource
    private BookService bookService;
    @Resource
    private LecService lecService;
    @Resource
    private UserBookLecService userBookLecService;

    @GetMapping("/TesTSUZE")
    public Result MyInfo(){
        return  Result.ok(cImgService.Info());
    }
    @GetMapping("Cimg")
    public Result Cimg(){
        return  Result.ok(cImgService.getImg());
    }
    @Resource
    CacheClearer cacheClearer;
    @Options(useCache = false)
    @GetMapping("Book")//这里是获取文章列表 总共有多少文章
    public  Result Book(){
        //cacheClearer.clearCache("TopOne.UserSystem.entity.index.Book");
        return bookService.queryList();
    }
    //这里根据文章id获取具体的文章 （可能后面还要再加AOP的拦截器来做每个用户的数据统计）

    @Options(useCache = false)
    @GetMapping("GetBookById")
    public Result  GetBookById(@RequestParam("id") Integer id)
    {
        Book book = bookService.queryById(id);
        return Result.ok(book);
    }
    @Options(useCache = false)
    @GetMapping("IsCollection")
    public Result  GetBookIsCollection(@RequestParam("id") Integer id,@RequestHeader("userId")String userid,@RequestParam("type")String type)
    {
        boolean Is =userBookLecService.IsCollection(id,userid,type);
        return Result.ok(Is);
    }
    @Options(useCache = false)
    @GetMapping("GetLecList")
    public Result GetLec(){
        return lecService.querLecList();
    }
    @Resource
    LecEssayServiceImpl lecEssayService;
    @Options(useCache = false)
    @GetMapping("GetLec")
    public Result GetLec(@RequestParam("id") Integer id){
        JSONObject data  =new JSONObject();
        //先判断 有没有开始
        //没开始 直接 是预约界面 根据id给讲座的所有数据
        Lec lec= lecService.queryById(id);
        if(lec==null){
            return Result.fail("不存在该讲座");
        }
        data.put("lec",lec);
        if (!lec.isAllowListen()){//不允许听 意味着已经开始
            //开始了，那就判断有没有写好讲座文章
            boolean isEssayOver=lecService.IsEssayOver(id);
            if (!isEssayOver){
                data.put("Essay",null);
            }
            else {
                JSONObject Essay = new JSONObject();
                //写好了给 没写好就是空
                LecEssay lecEssay = lecEssayService.queryLecEssayById(id);
                Essay.put("body", StrtoJSONUtil.convertToJSONObject(lecEssay.getBody()));
                Essay.put("id", lecEssay.getId());
                Essay.put("introduce", lecEssay.getIntroduce());
                Essay.put("title", lecEssay.getTitle());
                Essay.put("url", StrtoJSONUtil.convertToJSONObject(lecEssay.getUrl()));
                data.put("Essay", Essay);
            }
        }

        return Result.ok(data);
    }
    @Resource
   private LecHistoryService lecHistoryService;
    @Options(useCache = false)
    @PostMapping("BookingLec")
    public Result Booking(@RequestHeader("userId") String userId,@RequestBody String json){
        //Integer id,realName,phone
        JSONObject jsonObject = JSONObject.parseObject(json);
        Integer id = jsonObject.getInteger("id");
        String realName = jsonObject.getString("realName");
        String phone = jsonObject.getString("phone");
        lecService.book(id,realName,phone, userId);
        JSONObject IsBooking = new JSONObject();
        IsBooking.put("IsBooking",true);
        IsBooking.put("id",id);
        lecHistoryService.putHistory(id,userId);
        return Result.ok(IsBooking);
    }
    @Options(useCache = false)
    @GetMapping("IsBooking")
    public Result IsBooking(@RequestHeader("userId") String userId,@RequestParam("id")int id){
        return lecHistoryService.isBooking(id,userId);
    }

    }

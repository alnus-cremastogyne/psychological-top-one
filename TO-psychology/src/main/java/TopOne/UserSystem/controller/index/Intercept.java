package TopOne.UserSystem.controller.index;

import TopOne.dto.Result;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RequestMapping("/intercept")
@RestController
public class Intercept {
    @GetMapping("in")
    public Result Cimg(){
        return Result.ok();
    }
}

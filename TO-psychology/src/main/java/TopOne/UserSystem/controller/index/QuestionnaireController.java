package TopOne.UserSystem.controller.index;

import TopOne.dto.Result;
import TopOne.UserSystem.entity.index.Questionnaire;
import TopOne.UserSystem.service.QuestionnaireService;
import TopOne.UserSystem.service.Sentiment_analysisService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Options;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

@CrossOrigin
@RequestMapping("/Questionnaire")
@RestController
public class QuestionnaireController {
    @Resource
    QuestionnaireService questionnaireService;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @GetMapping("/info")
    @Options(useCache = false)
    public Result getList(){
        List<Questionnaire> list = questionnaireService.getList();
        JSONArray All=new JSONArray();
//        int i=0;
        for (Questionnaire questionnaire:list) {

            JSONObject data=new JSONObject();
            String ques = questionnaire.getQuestion();
            Long quesid = questionnaire.getId();
            String title = questionnaire.getTitle();
            String img = questionnaire.getImg();
            String readcout = questionnaire.getReadcout();
            JSONArray quesJson = JSON.parseArray(ques);
            data.put("id",quesid);
            data.put("ques",quesJson);
            data.put("title",title);
            data.put("img",img);
            data.put("readcount",readcout);
//            String name="data"+i;
            All.add(data);
//            i++;
        }

        return Result.ok(All);

    }
    //2.ans
    @PostMapping("/ans")
    public Result PostAnswer(@RequestHeader("userId") String userId,@RequestBody String answer) {
        String token = "Que:" + userId;
        //System.out.println(answer);
        stringRedisTemplate.opsForValue().set(token, answer, 30, TimeUnit.MINUTES);
        return Result.ok();
    }
        //根据ans计算情绪 心情 压力  区间 返回给前端做曲线图
//    20个项目，9个项目属于心情表（ 1，2，3，4，5 , 6，7，8 ,9 ），mood
//    6个项目属于情绪表（10， 11， 12， 13， 14， 15），sentiment
//    5个项目属于压力表（16， 17， 18， 19， 20）。pressure

    @Resource
    Sentiment_analysisService service;
    @PostMapping("/ComeOut")
    public Result ComeOut(@RequestHeader("userId") String userId){
        String token = "Que:" + userId;
        //redis里面存的是String格式的数字
        String jsonStr = stringRedisTemplate.opsForValue().get(token);
        JSONArray answers  = JSON.parseArray(jsonStr);
        // 清空答题记录
        //stringRedisTemplate.delete(token);
        //开始操作 并直接返回 数据
        //TODO 如何完成更好的心情监测 待定具体更好的做法

        return service.comeOut(userId,answers);
    }
}



package TopOne.UserSystem.controller.Me;

import TopOne.UserSystem.entity.User.User;
import TopOne.UserSystem.entity.User.journal;
import TopOne.UserSystem.service.Sentiment_analysisService;

import TopOne.UserSystem.service.TestHistoryService;
import TopOne.UserSystem.service.journalService;
import TopOne.utils.UserHolder;
import cn.hutool.json.JSONObject;
import TopOne.dto.Result;
import TopOne.UserSystem.service.IUserService;
import cn.hutool.json.JSONUtil;
import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipaySystemOauthTokenRequest;
import com.alipay.api.response.AlipaySystemOauthTokenResponse;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Options;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static TopOne.utils.SystemConstants.*;


/**
 * <p>
 * 前端控制器
 * </p>
 /**
 *@author suze
 *@date 2023-10-25
 *@time 15:17
 **/


@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private IUserService userService;
    /**
     * 登录功能
     *
     */
    //TODO
    @CrossOrigin
    @PostMapping("/wechat/login")     //从请求头中获取的token
    public ResponseEntity<?> wechatLogin(@RequestHeader("token") String token,@RequestBody Map<String, String> wxLoginInfo) {
        System.err.println("Token:"+token);
        String code = wxLoginInfo.get("code");
        // 调用微信登录凭证校验接口，获取用户信息AppID 和 AppSecret
        String appId = "wx93bd45ef6fa7e462";
        String appSecret = "8e7bb87df10ce8955297f1e050f40986";
        // 发送HTTP请求获取用户信息
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://api.weixin.qq.com/sns/jscode2session?appid=" + appId + "&secret=" + appSecret + "&js_code=" + code + "&grant_type=authorization_code";
        String jsonResponse = restTemplate.getForObject(url, String.class);
        // 解析返回的JSON数据，提取OpenID和SessionKey
        //System.out.println(jsonResponse);//看看里面的名称是否对的上
        JSONObject jsonObject = new JSONObject(jsonResponse);
        String openid = jsonObject.getStr("openid");
        String sessionKey = jsonObject.getStr("session_key");
        String name = wxLoginInfo.get("nickName");
        String icon = wxLoginInfo.get("avatarUrl");
        //String phone = wxLoginInfo.get("mobile");
        // 将用户信息返回给前端
        Map<String, String> userInfo = new HashMap<>();
        userInfo.put("openid", openid);
        userInfo.put("sessionKey", sessionKey);
        //做一个管理用户的操作  返回token
        //System.out.println(name+","+icon);
        String Token = userService.login(openid, name, icon,sessionKey,token);
        userInfo.put("token", Token);
        //open id要作为user的主键 然后调用user服务类操作user数据库并且做登录状态的缓存
        //用户登录成功后，服务端生成一个唯一的 token，并将其存储到数据库或缓存中，
        return ResponseEntity.ok(userInfo);
    }
      //从请求头中获取的token
    /*
     * (支付宝) 换取授权访问令牌
     * @param grantType 必选 授权方式
     * @param code 可选 授权码
     * @param refreshToken 可选 刷新令牌
     * @return
     */
    //这个东西是刷新令牌，上次换取访问令牌时得到。本参数在 grant_type 为 authorization_code 时不填；
    private String refreshToken ="";
    @CrossOrigin
    @PostMapping("/zfb/login")//String grantType,String code,String refreshToken
    private ResponseEntity<?> getAuthToken(@RequestHeader("token") String token,@RequestBody String json) throws AlipayApiException {
        //获取String code,String refreshToken
        JSONObject jsonObject = JSONUtil.parseObj(json);
        String grantType = jsonObject.getStr("grantType");
        String code = jsonObject.getStr("code");
        //String refreshToken = jsonObject.getStr("refreshToken");
        AlipayClient alipayClient = new DefaultAlipayClient(
                "https://openapi.alipay.com/gateway.do",  // 请求支付宝网关地址,建议定义为常量保存
                ALIBABA_APP_ID,     					  // 小程序APPID,建议存储在配置中心中,动态获取.
                ALIBABA_SECRET_KEY ,				      // 小程序密钥,建议存储在配置中心中,动态获取.
                "json",									  // 仅支持JSON
                "utf-8",								  // 请求使用的编码格式，如utf-8,gbk,gb2312等
                ALIBABA_PUBLIC_KEY,						  // 小程序公钥,建议存储在配置中心中,动态获取.
                "RSA2");								  // 商户生成签名字符串所使用的签名算法类型，推荐使用RSA2

        AlipaySystemOauthTokenRequest request = new AlipaySystemOauthTokenRequest();
        // 构建请求参数
        request.setGrantType(grantType);
        request.setCode(code);
        if(ObjectUtils.isNotEmpty(grantType)){
            request.setGrantType(grantType);
        }else if(ObjectUtils.isNotEmpty(refreshToken)){
            request.setRefreshToken(refreshToken);
        }
        // 响应
        AlipaySystemOauthTokenResponse response = alipayClient.execute(request);
        if(response.isSuccess()){
            log.info("[AlipaySystemOauthTokenResponse] >>>> Succeed!");
            System.out.println(response.getBody());
            Map<String, String> userInfo = new HashMap<>();
            userInfo.put("openid", response.getOpenId());
            userInfo.put("sessionKey", response.getRefreshToken());
            String Token = userService.login(
                    response.getOpenId(),
                    "支付宝用户",
                    "https://thirdwx.qlogo.cn/mmopen/vi_32/POgEwh4mIHO4nibH0KlMECNjjGxQUq24ZEaGT4poC6icRiccVGKSyXwibcPq4BWmiaIGuG1icwxaQX6grC9VemZoJ8rg/132",
                    response.getRefreshToken(),
                    token);
            userInfo.put("token", Token);
            return ResponseEntity.ok(userInfo);
        } else {
            log.error("[AlipaySystemOauthTokenResponse] = Fail! Cause:{}",response.getSubMsg());
            throw new AlipayApiException("获取支付宝信息失败,请稍后再试!");
        }
    }


    @PostMapping("/Psychological")
    public Result setdescription(@RequestBody String description,@RequestBody String token){
        //这里要用区块链的同态加密算法来实现对用户的敏感信息的保存
        return userService.setPsychological(description,token);
    }
    /**
     * 登出功能
     * @return 无
     */
    @PostMapping("/logout")
    public Result logout(@RequestHeader("token") String token){
        return userService.logout(token);
    }
    @Resource
    Sentiment_analysisService analysisService;
    @GetMapping("/me")
    @Options(useCache = false)
    public Result me(){
        //User user = userService.getById(1);
        User user = UserHolder.getUser();
        com.alibaba.fastjson.JSONObject data=new com.alibaba.fastjson.JSONObject();
        //TODO 这里要返回那三个心理值给前端
        data.put("user",user);
        if(user!=null){
            data.put("Value",analysisService.GetByid(user.getOpenId()));
        }
        return Result.ok(data);
    }
    @GetMapping("/ChangeIcon")
    @Options(useCache = false)
    public Result ChangeIcon(@RequestParam("url") String url,@RequestHeader("userId") String id){
        return userService.setIcon(url,id);
    }
    @GetMapping("/WriteWord")
    @Options(useCache = false)
    public Result WriteWord(@RequestParam("word") String word,@RequestHeader("userId") String id){
        String writeWord = userService.writeWord(word, id);
        return Result.ok(writeWord);
    }
    @GetMapping("/GetWord")
    @Options(useCache = false)
    public Result GetWord(@RequestHeader("userId") String id){
        String word = userService.getWord(id);
        return Result.ok(word);
    }
    @PostMapping("/me/upData")
    @Options(useCache = false)
    public Result upDataMe(@RequestBody User user,@RequestHeader("token") String token,@RequestHeader("userId") String userId){

        User me = userService.upDataMe(user, token, userId);
        if (me==null){
            return Result.fail("更新失败");
        }
        return Result.ok(me);
    }


    @GetMapping("/Collection")
    @Options(useCache = false)//这里的EssayId是一个数字  choose就是 收藏或取消收藏
    public Result Collection(@RequestHeader("userId") String userId, @RequestParam("id") int id, @RequestParam("type")String type )
    {
        //这里把收藏和取消收藏的操作全部 放在Redis里去操作 考虑有人手贱的情况 而且这样反应也会更快
        return userService.collection(userId, id,type);
    }

    @GetMapping("/GetCollection")
    @Options(useCache = false)
    public Result GetCollection(@RequestHeader("userId") String userId,@RequestParam("type")String type)
    {
        List list = userService.GetCollection(userId, type);
        if (list != null) {
            return Result.ok(list);
        }
        return  Result.ok("没有收藏讲座");
    }
    @Resource
    TestHistoryService testHistoryService;
    @GetMapping("/GetHistory")
    @Options(useCache = false)
    public Result GetHistory(@RequestHeader("userId") String userId)
    {
        return testHistoryService.getHistory(userId);
    }


    //    上传日志的情绪值，内容，日期，userID保存到数据库，拥有修改功能
    @Resource
    private journalService Servicejournal;

    @PostMapping("/upload-log")
    public Result uploadLog(@RequestBody journal log, @RequestHeader("userId") String user_id) {
        return Servicejournal.uploadLog(user_id,
                log.getValueEmotion(),
                log.getWriteContent(),
                log.getCreatedAt());
    }

    @GetMapping("/get-log")
    public Result getlog(@RequestHeader("userId") String user_id,@RequestParam("date")String date){
        return Servicejournal.getList(user_id, date);
    }

}

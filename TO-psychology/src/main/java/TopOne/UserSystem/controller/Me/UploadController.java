package TopOne.UserSystem.controller.Me;

import TopOne.UserSystem.service.IUserService;
import TopOne.utils.AliyunUtil;
import TopOne.dto.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.UUID;

/**
 /**
 *@author suze
 *@date 2023-10-25
 *@time 15:17
 **/

@Slf4j
@RestController
@RequestMapping("upload")
public class UploadController {
    @Resource
    IUserService userService;

    @PostMapping("blog")
    public Result uploadImage(@RequestParam("file") MultipartFile image,@RequestParam("id") String id) {
        try {
            // 获取原始文件名称
            String originalFilename = image.getOriginalFilename();
            // 生成新文件名
            String fileName = UUID.randomUUID().toString()+originalFilename.substring(originalFilename.lastIndexOf("."));
            System.out.println(fileName);
            // 保存文件  阿里云的OSS工具类根据SDK搞定他
            //image.transferTo(new File(SystemConstants.IMAGE_UPLOAD_DIR, fileName));
            String url = AliyunUtil.Upload(image.getInputStream(), fileName);
            System.out.println("4");
            // 返回结果
            log.debug("文件上传成功，{}", fileName);
            //放url 并返回
            return userService.setIcon(url,id);
        } catch (IOException e) {
            throw new RuntimeException("文件上传失败", e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    @PostMapping("file")
    public Result upload(@RequestParam("file") MultipartFile image) {
        try {
            // 获取原始文件名称
            String originalFilename = image.getOriginalFilename();
            // 生成新文件名
            String fileName = UUID.randomUUID().toString()+originalFilename.substring(originalFilename.lastIndexOf("."));
            System.out.println(fileName);
            // 保存文件  阿里云的OSS工具类根据SDK搞定他
            //image.transferTo(new File(SystemConstants.IMAGE_UPLOAD_DIR, fileName));
            String url = AliyunUtil.Upload(image.getInputStream(), fileName);
            System.out.println("4");
            // 返回结果
            log.debug("文件上传成功，{}", fileName);
            //放url 并返回
            return Result.ok(url);
        } catch (IOException e) {
            throw new RuntimeException("文件上传失败", e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
//    @GetMapping("/blog/delete")
//    public Result deleteBlogImg(@RequestParam("name") String filename) {
//        //TODO 根据OSS重写
//        File file = new File(SystemConstants.IMAGE_UPLOAD_DIR, filename);
//
//        if (file.isDirectory()) {
//            return Result.fail("错误的文件名称");
//        }
//
//        return Result.ok();
//    }
    @GetMapping("/blog/delete")
    public Result deleteBlogImg(@RequestParam("name") String filename) {
//        OSS ossClient = new OSSClientBuilder().build(SystemConstants.ENDPOINT, SystemConstants.ACCESS_KEY_ID, SystemConstants.ACCESS_KEY_SECRET);
//        boolean exists = ossClient.doesObjectExist(SystemConstants.BUCKET_NAME, filename);
//        if (!exists) {
//            return Result.fail("文件不存在");
//        }
//        ossClient.deleteObject(SystemConstants.BUCKET_NAME, filename);
//        ossClient.shutdown();
        return Result.ok();
    }

}

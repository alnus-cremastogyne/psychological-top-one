package TopOne.UserSystem.controller.Aitalk;

import TopOne.dto.Result;
import TopOne.UserSystem.entity.Aitalk.MBTI.Answer;
import TopOne.UserSystem.entity.Aitalk.MBTI.Mbti;
import TopOne.UserSystem.entity.Aitalk.MBTI.MbtiResult;
import TopOne.UserSystem.entity.Aitalk.MBTI.Talks;
import TopOne.UserSystem.service.MRService;
import TopOne.UserSystem.service.TestHistoryService;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Options;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author suze
 * @date 2023/11/6
 * @time 11:41
 **/


@CrossOrigin
@RequestMapping("/MBTI")
@RestController
public class MbtiController {
    private Talks talk = new Talks();
    private Mbti  mbti=new Mbti();
    private MbtiComputer mbtiComputer=new MbtiComputer();
    private List<Answer> answerList = BeanUtil.copyToList(JSONUtil.parseArray(talk.getAnswer()),Answer.class);
    private JSONArray Description=JSON.parseArray(mbti.getDescription());
    private JSONArray ChineseName=JSON.parseArray(mbti.getChineseName());
    private String ques=talk.getQues();
    JSONArray quesJson = JSON.parseArray(ques);

  private String options=talk.getOptions();
    JSONArray optionsJson = JSON.parseArray(options);
//    下面这是初始化的两个接口
    @GetMapping("/Talks")
    public Result GetTalks(){

        //创建一个JSON的object  然后把三个已经JSON序列化好的三个对象放进去
        JSONObject data = new JSONObject();
        data.put("ques", quesJson);
        data.put("Options", optionsJson);
        return Result.ok(data);

    }

    @GetMapping("/Answer")
    public Result GetAnswer(@RequestHeader("userId")String userId){
        System.err.println(userId);
        String token = "MBTI:" + userId;
        //创建一个JSON的object  然后把三个已经JSON序列化好的三个对象放进去
        JSONObject data = new JSONObject();
        data.put("Answer", answerList);
        // 从Redis中获取答题记录
        List<String> answerJsons = stringRedisTemplate.opsForList().range(token, 0, -1);
        // 反序列化JSON字符串为答案对象列表
        data.put("history",answerJsons);
        return Result.ok(data);
    }


//    下面是当用户选择答案
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private TestHistoryService testHistoryService;

    private PersonalityTest people =new  PersonalityTest();
    @PostMapping("/UserWriteAns")
    public Result PostAnswer(@RequestHeader("userId") String userId,@RequestBody Answer answer){
        String token = "MBTI:" + userId;
        // 序列化答案对象为JSON字符串
        String answerJson = JSON.toJSONString(answer);
        // 如果用户ID不存在，则创建新的答题记录
        // 存储答案到相应用户的答题记录中
        stringRedisTemplate.opsForList().rightPush(token, answerJson);
        //设置一个一天的缓存
        stringRedisTemplate.expire(token, 1, TimeUnit.DAYS);
        return Result.ok();
    }
    @PostMapping("/over")
    @Options(useCache = false)
    public Result OverAnswer(@RequestHeader("userId") String userId){
        //最后我们将算出来的结果还回去
//         PersonalityTest test = new PersonalityTest(0, 0, 0, 0, 0, 0, 0, 0);
//        String result = test.jud();
        // 检查Redis中是否存在指定的userID
        String token = "MBTI:" + userId;
        boolean keyExists = stringRedisTemplate.hasKey(token);
        if (!keyExists){
            System.err.println(token);
            return Result.fail("你的测试记录已过期请重新测试！");
        }
        // 从Redis中获取答题记录
        List<String> answerJsons = stringRedisTemplate.opsForList().range(token, 0, -1);
        // 反序列化JSON字符串为答案对象列表
        for (String answerJson : answerJsons){

            //反序列化为Answer对象
            Answer answer = JSON.parseObject(answerJson, Answer.class);
            //每一轮问题都会进行统计 存到people里面
            people = mbtiComputer.statistics(answer);

        }
        String result = people.jud();
        JSONObject data = new JSONObject();
        data.put("result", result);
        int cout = 0;
        JSONObject descriptionObj = new JSONObject();
        for (int i = 0; i < Description.size(); i++) {
            JSONObject description = Description.getJSONObject(i);

            String type = description.getString("类型");
            if (result.equals(type)) {
                // 找到匹配的类型
                descriptionObj=description;
                cout=i;
                break;
            }
        }
        // 调用getter方法获取四个值
        double rEI = people.getrEI();
        double rSN = people.getrSN();
        double rTF = people.getrTF();
        double rJP = people.getrJP();

// 将这些值封装在JSONObject中
        JSONObject proportion = new JSONObject();
        proportion.put("rEI", rEI);
        proportion.put("rSN", rSN);
        proportion.put("rTF", rTF);
        proportion.put("rJP", rJP);

        String  chineseName = (String) ChineseName.get(cout);
        data.put("Description", descriptionObj);
        data.put("ChineseName", chineseName);
        data.put("proportion", proportion);
        data.put("time", LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
       //stringRedisTemplate.delete(token);
        testHistoryService.putMbti(userId,JSONObject.toJSONString(data));
        return Result.ok(data);

    }
    @Resource
    MRService mrService;
    @Options(useCache = false)
    @GetMapping("/result")
    public Result getResult(@RequestParam("type") String type){
        MbtiResult result = mrService.getByType(type);
        String replace = result.getResult().replace("\n", "");
        return Result.ok(replace);
    }



}

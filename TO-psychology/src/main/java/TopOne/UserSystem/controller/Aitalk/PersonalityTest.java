package TopOne.UserSystem.controller.Aitalk;

/**
 * @author suze
 * @date 2023/11/8
 * @time 16:51
 **/
public class PersonalityTest {

    public int E;
    public int I;
    public int S;
    public int N;
    public int T;
    public int F;
    public int J;
    public int P;

    public PersonalityTest() {

    }

    public PersonalityTest(int E, int I, int S, int N, int T, int F, int J, int P) {
        this.E = E;
        this.I = I;
        this.S = S;
        this.N = N;
        this.T = T;
        this.F = F;
        this.J = J;
        this.P = P;
    }
    private double rEI;
    private double rSN;
    private double rTF;
    private double rJP;
    public String jud() {
        rEI = (double) E / (E + I);
        rSN = (double) S / (S + N);
        rTF = (double) T / (T + F);
        rJP = (double) J / (J + P);



//        System.out.println(rEI);
//        System.out.println(rSN);
//        System.out.println(rTF);
//        System.out.println(rJP);

        String result = "";
        String res1 = rEI > 0.5 ? "E" : "I";
        String res2 = rSN > 0.5 ? "S" : "N";
        String res3 = rTF > 0.5 ? "T" : "F";
        String res4 = rJP > 0.5 ? "J" : "P";

        result = res1 + res2 + res3 + res4;

        return result;
    }

    public double getrEI() {
        return rEI;
    }



    public double getrSN() {
        return rSN;
    }



    public double getrTF() {
        return rTF;
    }



    public double getrJP() {
        return rJP;
    }


}

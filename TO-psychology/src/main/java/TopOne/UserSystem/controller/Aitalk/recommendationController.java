package TopOne.UserSystem.controller.Aitalk;


import TopOne.dto.Result;
import TopOne.UserSystem.entity.Aitalk.Recommendation;
import TopOne.dto.Result;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Options;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RequestMapping("/recommendation")
@RestController
public class recommendationController {

    @PostMapping("/info")
    @Options(useCache = false)
    public Result info(){
        JSONObject data=new JSONObject();
        Recommendation recommendation=new Recommendation();
        List<String> newRecom = recommendation.getNewQues();
        JSONArray RecomArry=new JSONArray();
        for (String recom :newRecom) {
            RecomArry.add(recom);
        }
        data.put("recommendation",RecomArry);
        return Result.ok(RecomArry);
    }

}

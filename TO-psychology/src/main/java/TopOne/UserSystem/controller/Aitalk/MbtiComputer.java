package TopOne.UserSystem.controller.Aitalk;

import TopOne.UserSystem.entity.Aitalk.MBTI.Answer;

/**
 * @author suze
 * @date 2023/11/8
 * @time 16:38
 **/
public class MbtiComputer {
    private PersonalityTest thisper=new  PersonalityTest();

    public PersonalityTest statistics(Answer answer){
        String theDem = answer.getType();
        boolean bool = answer.getBool();
        boolean btype = answer.getBtype();
        if ((btype && bool) || (!btype && !bool)) {
            switch (theDem) {
                case "E":
                    thisper.E++;
                    break;
                case "S":
                    thisper.S++;
                    break;
                case "T":
                    thisper.T++;
                    break;
                case "J":
                    thisper.J++;
                    break;
                default:
                    System.out.println("no mbti switch");
            }
        } else {
            switch (theDem) {
                case "E":
                    thisper.I++;
                    break;
                case "S":
                    thisper.N++;
                    break;
                case "T":
                    thisper.F++;
                    break;
                case "J":
                    thisper.P++;
                    break;
                default:
                    System.out.println("no mbti switch");
            }
        }

       return thisper;
    }
}

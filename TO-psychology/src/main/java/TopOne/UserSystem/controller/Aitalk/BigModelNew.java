package TopOne.UserSystem.controller.Aitalk;

/**
 * @author suze
 * @date 2023/10/30
 * @time 20:44
 **/

import TopOne.utils.AiUtils.WordUtils;
import cn.hutool.json.JSONUtil;
import TopOne.dto.Result;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.*;


@RestController
@CrossOrigin
@RequestMapping("/chat")
public class BigModelNew extends WebSocketListener {
    //返回的json结果拆解
    class JsonParse {
        Header header;
        Payload payload;
    }

    class Header {
        int code;
        int status;
        String sid;
    }

    class Payload {
        Choices choices;
    }

    class Choices {
        List<Text> text;
    }

    class Text {
        String role;
        String content;
    }
    class RoleContent{

        String role;
        String content;

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
    private StringRedisTemplate stringRedisTemplate;//注入Redis
    @Autowired//这里注入Redis
    public BigModelNew(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
    }

    //固定的部分
    public static final String hostUrl = "https://spark-api.xf-yun.com/v3.1/chat";
    public static final String appid = "3d1ae3d9";
    public static final String apiSecret = "NjBlNjM3NzBhMTY0YTk4YTZlYzNkNGY2";
    public static final String apiKey = "311b002a45a6d521068303577dbf96dc";
    //对话历史存储集合
    public  List<RoleContent> historyList=new ArrayList<>(); // 这个是临时的 每次用完在线程中要注销他

    // 将对话历史存储到 Redis
    public void saveHistory(String  history,String id) {
        // 设置有效期为 2 小时
        stringRedisTemplate.opsForValue().set("id:" + id + ":history", history, 2, TimeUnit.HOURS);
    }
    @RequestMapping("/SaveHistory")
    public Result SaveHistory(@RequestParam("userId") String id, @RequestBody String history)  {
        saveHistory(history,id);
        return Result.ok("保存历史记录成功有效时间:2h");
    }
    // 从 Redis 中获取对话历史
    public List<RoleContent> getHistory(String userId) {
        String historyStr = stringRedisTemplate.opsForValue().get("id:" + userId + ":history");
        if (historyStr==null){
            return null;
        }
        return JSONUtil.toList(JSONUtil.parseArray(historyStr), RoleContent.class);
    }
    //用于获取历史聊天记录
    @RequestMapping("/history")
    public Result history(@RequestParam("userId") String id)  {

        String history = stringRedisTemplate.opsForValue().get("id:" + id + ":history");
        if (history == null) {
            return Result.fail("没有找到历史记录");
        }

        JSONArray jsonObject = JSON.parseArray(history);
//        String jsonString = JSON.toJSONString(jsonObject);
        return Result.ok(jsonObject);
    }




    public static String totalAnswer=""; // 大模型的答案汇总

    // 可以写原始问题
    public static  String NewQuestion = "";
    public WordUtils wordUtils=new WordUtils();
    //搞个线程池 用来执行任务
    public ThreadPoolExecutor pool = new ThreadPoolExecutor(13, 13, 1,
            TimeUnit.MINUTES, new ArrayBlockingQueue<>(6),
            Executors.defaultThreadFactory(), new ThreadPoolExecutor.AbortPolicy());
    @RequestMapping("/get")
    public DeferredResult<Result> get(@RequestParam("question") String question, @RequestParam("id") String id) {
        //这里要先对文本进行预处理
        String questionOK = wordUtils.checkQuestionLength(question);
        //创建了一个DeferredResult<Result>对象，并将其返回给前端。在异步任务执行完毕后，
        // 通过调用deferredResult.setResult(result)方法将结果设置到DeferredResult对象中，从而实现异步返回结果给前端。
        DeferredResult<Result> deferredResult = new DeferredResult<>();
        CompletableFuture.supplyAsync(() -> {
            try {
                callable callable = new callable(questionOK, id);
                String answer = callable.call();
                Result result = Result.ok(answer);
                System.out.println("接口："+answer);
                return result;
            } catch (Exception e) {
                Result result = Result.fail(e.getMessage());
                return result;
            }
        }, pool).whenComplete((result, throwable) -> {
            if (throwable != null) {
                result = Result.fail(throwable.getMessage());
                deferredResult.setResult(result);

            } else {
                deferredResult.setResult(result);

            }
        });
        return deferredResult;
    }
    public class callable implements Callable<String>{
        private  String  id;
        private String question;
        public callable(String question,String id) {
            //这里处理一下userId的长度 因为讯飞那边限制了
            if (id.length() >= 30) {
                id= id.substring(0, 30);
            }
            this.question = question;
            this.id=id;
        }
        @Override
        public String call() throws Exception {
            String answer =main(question,id);
            //System.out.println(answer);
            answer = JSONUtil.toJsonStr(answer);
            System.out.println("call");
            botText.content="";//清空
            //缓存历史对话  时效两小时
            //String historyStr = JSONUtil.toJsonStr();
            //stringRedisTemplate.opsForValue().set("id:"+id+":history", historyStr,2,TimeUnit.HOURS);
            return answer;
        }
    }
    // 主函数
    public String main(String newQuestion,String userid) throws Exception {
        // 个性化参数入口，如果是并发使用，可以在这里模拟
        System.out.println(totalFlag);
        if(totalFlag){
            totalFlag=false;
            NewQuestion=newQuestion;
            // 构建鉴权url
            String authUrl = getAuthUrl(hostUrl, apiKey, apiSecret);
            OkHttpClient client = new OkHttpClient.Builder().build();
            String url = authUrl.toString().replace("http://", "ws://").replace("https://", "wss://");
            System.out.println("c");
            Request request = new Request.Builder().url(url).build();
            totalAnswer="";
//                    WebSocket webSocket = client.newWebSocket(request, new BigModelNew(i + "",false));
            //这里创建了大模型的新对象  实际上那些发送请求获取答案的操作都是在这个线程中做的
            System.out.println("a");
            BigModelNew bigModelNew = null;
            if (getHistory(userid)!=null){
                bigModelNew=new BigModelNew(userid, false,getHistory(userid),stringRedisTemplate);
            }
            else {
                bigModelNew=new BigModelNew(userid, false,historyList,stringRedisTemplate);
            }
            System.out.println("b");
            // 等待 WebSocket 的 run() 方法执行完毕
            int maxWaitTime = 10000; // 最大等待时间，单位：毫秒
            int currentWaitTime = 0; // 当前已等待的时间，单位：毫秒
            int waitInterval = 1000;// 每次等待的时间间隔，单位：毫秒
            WebSocket webSocket = client.newWebSocket(request, bigModelNew);
            System.out.println(maxWaitTime);
            while (currentWaitTime < maxWaitTime) {
                if (bigModelNew.getBotContent().equals("")) {
                    // run() 方法还未执行完毕，可以进行一些其他操作或等待一段时间
                    Thread.sleep(waitInterval);
                    System.out.println("正在执行线程"+Thread.currentThread().getName()+"...等待时间还剩："+(maxWaitTime-currentWaitTime));
                    currentWaitTime += waitInterval;
                } else {
                    // run() 方法已执行完毕，获取 bot.content 值并进行后续操作
                    //System.out.println("run执行完毕");
                    return bigModelNew.getBotContent();
                    //System.out.println(botText.content);
                    // ...
                }
            }
        }
        totalFlag=true;
        return "网络开了点小差 试试重新发送你的消息吧";
    }
    // 构造函数
    public BigModelNew(@org.springframework.beans.factory.annotation.Value("${userId}") String userId
            ,@Value("${wsCloseFlag}") Boolean wsCloseFlag
            ,@Value("${HistoryList}")List<RoleContent> HistoryList
            ,@Value("${stringRedisTemplate}") StringRedisTemplate stringRedisTemplate) {
        this.userId = userId;
        this.wsCloseFlag = wsCloseFlag;
        this.historyList=HistoryList;
        this.stringRedisTemplate = stringRedisTemplate;
    }




    public static final Gson gson = new Gson();

    // 个性化参数
    private String userId;
    private Boolean wsCloseFlag;

    private static Boolean totalFlag=true; // 控制提示用户是否输入
   


    //写一个结构专门来存机器的回答
    public static class BotText{
        String content=" ";
    }
    public static BotText botText=new BotText();

    //一个结构专门存历史记录
    public static class History{
        String text;

    }

    //告诉控制器输出完没有
    public static Boolean outputFlag=true;
    private String botContent = "";
    //一个很关键的函数 用于得到botContent
    public String getBotContent() {
        return botContent;
    }


    public boolean canAddHistory(){  // 由于历史记录最大上线1.2W左右，需要判断是能能加入历史

        int history_length=0;
        for(RoleContent temp:historyList){
            history_length=history_length+temp.content.length();
        }
        if(history_length>12000){
            historyList=new ArrayList<>();
            return false;
        }else{
            return true;
        }

    }

    // 线程来发送音频与参数
    class MyThread extends Thread {
        private WebSocket webSocket;
        private  String newAnswer;
        public MyThread(WebSocket webSocket) {
            this.webSocket = webSocket;
        }

        public void run() {
            try {
                JSONObject requestJson=new JSONObject();
                JSONObject header=new JSONObject();  // header参数
                header.put("app_id",appid);
                header.put("uid",userId);//这里放userId
                JSONObject parameter=new JSONObject(); // parameter参数
                JSONObject chat=new JSONObject();
                chat.put("domain","generalv3");
                chat.put("temperature",0.6);
                chat.put("max_tokens",8192);
                parameter.put("chat",chat);
                JSONObject payload=new JSONObject(); // payload参数
                JSONObject message=new JSONObject();
                JSONArray text=new JSONArray();
                // 历史问题获取
                if(historyList.size()>0){
                    for(RoleContent tempRoleContent:historyList){
                        text.add(JSON.toJSON(tempRoleContent));
                    }
                }
                // 最新问题
                RoleContent roleContent=new RoleContent();
                roleContent.role="user";
                roleContent.content=NewQuestion;
                text.add(JSON.toJSON(roleContent));
//                System.err.println("text:");
                historyList.add(roleContent);
//                historyList.forEach(System.out::println);
                //saveHistory(historyList);//在这里就把历史记录存到Redis了 就可以清空该线程的历史记录List了
                //historyList.clear();
                message.put("text",text);
                payload.put("message",message);
                requestJson.put("header",header);
                requestJson.put("parameter",parameter);
                requestJson.put("payload",payload);
//                  System.err.println(requestJson); // 可以打印看每次的传参明细
                webSocket.send(requestJson.toString());
                // 等待服务端返回完毕后关闭
                while (true) {
                    // System.err.println(wsCloseFlag + "---");
                    Thread.sleep(200);
                    if (wsCloseFlag) {
                        break;
                    }
                }
                webSocket.close(1000, "");
//                System.out.println("answer"+botText.content);
            } catch (Exception e) {
                e.printStackTrace();
            }finally {
                botContent=botText.content;
            }
        }
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        super.onOpen(webSocket, response);
        //System.out.print("AI：");
        MyThread myThread = new MyThread(webSocket);
        myThread.start();
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        // System.out.println(userId + "用来区分那个用户的结果" + text);
        JsonParse myJsonParse = gson.fromJson(text, JsonParse.class);
        //System.out.println("AI：" + gson.toJson(myJsonParse.payload));
        if (myJsonParse.header.code != 0) {
            System.out.println("发生错误，错误码为：" + myJsonParse.header.code);
            System.out.println("本次请求的sid为：" + myJsonParse.header.sid);
            webSocket.close(1000, "");
        }
        List<Text> textList = myJsonParse.payload.choices.text;

        for (Text temp : textList) {
            //System.out.println("这里存了机器的话"+temp.content);
            botText.content=botText.content+temp.content;//这里存机器的话
            totalAnswer=totalAnswer+temp.content;
        }
        //System.out.println("answeraaaa:"+botText.content);//这里能够打印 但是打印很多次说明他调用了很多次
//        botText.content=totalAnswer;
        if (myJsonParse.header.status == 2) {
            // 可以关闭连接，释放资源
//            System.out.println();
//            System.out.println("*************************************************************************************");
            if(canAddHistory()){
                RoleContent roleContent=new RoleContent();
                roleContent.setRole("assistant");
                roleContent.setContent(totalAnswer);
                historyList.add(roleContent);
                //System.out.println("OnMessage:"+historyList.toString());
                //String jsonString = JSON.toJSONString(historyList, SerializerFeature.PrettyFormat);
                //history.text=jsonString;//这里已经把历史记录给保存了
                //在这里答案已经输出完了，就应该outputFinished = true;
//                output=botText.content;
//                myHistory=history.text;
                outputFlag=true;
                System.out.println("AI:answer"+botText.content);
            }else{
                RoleContent roleContent=new RoleContent();
                roleContent.setRole("assistant");
                roleContent.setContent(totalAnswer);
                historyList.add(roleContent);
            }
            //saveHistory(historyList);//在这里就把历史记录存到Redis了 就可以清空该线程的历史记录List了
            historyList.clear();
            wsCloseFlag = true;//只有等消息传过来了  才能够结束
            totalFlag=true;
        }

    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        super.onFailure(webSocket, t, response);
        if (null != response) {
            int code = response.code();
                System.out.println("onFailure code:" + code);
            try {
                System.out.println("onFailure body:" + response.body().string());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            if (101 != code) {
                System.out.println("connection failed");
                System.exit(0);
            }
        }
    }


    // 鉴权方法
    public static String getAuthUrl(String hostUrl, String apiKey, String apiSecret) throws Exception {
        URL url = new URL(hostUrl);
        // 时间
        SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        String date = format.format(new Date());
        // 拼接
        String preStr = "host: " + url.getHost() + "\n" +
                "date: " + date + "\n" +
                "GET " + url.getPath() + " HTTP/1.1";
//        System.err.println(preStr);
        // SHA256加密
        Mac mac = Mac.getInstance("hmacsha256");
        SecretKeySpec spec = new SecretKeySpec(apiSecret.getBytes(StandardCharsets.UTF_8), "hmacsha256");
        mac.init(spec);
        byte[] hexDigits = mac.doFinal(preStr.getBytes(StandardCharsets.UTF_8));

        // Base64加密
        String sha = Base64.getEncoder().encodeToString(hexDigits);
//        System.err.println(sha);
        // 拼接
        String authorization = String.format("api_key=\"%s\", algorithm=\"%s\", headers=\"%s\", signature=\"%s\"", apiKey, "hmac-sha256", "host date request-line", sha);
        // 拼接地址
        HttpUrl httpUrl = Objects.requireNonNull(HttpUrl.parse("https://" + url.getHost() + url.getPath())).newBuilder().//
                addQueryParameter("authorization", Base64.getEncoder().encodeToString(authorization.getBytes(StandardCharsets.UTF_8))).//
                addQueryParameter("date", date).//
                addQueryParameter("host", url.getHost()).//
                build();

//        System.err.println(httpUrl.toString());
        return httpUrl.toString();
    }




}
package TopOne.UserSystem.controller.Aitalk;
import TopOne.dto.Result;
import TopOne.UserSystem.entity.Aitalk.Regular.Ans;
import TopOne.UserSystem.entity.Aitalk.Regular.Que;
import TopOne.UserSystem.entity.Aitalk.Regular.TestResult;
import TopOne.UserSystem.entity.Aitalk.RegularQue;
import TopOne.UserSystem.service.RegularQueService;
import TopOne.UserSystem.service.TestHistoryService;
import TopOne.UserSystem.service.TestResultService;
import TopOne.utils.StrtoJSONUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Options;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

@CrossOrigin
@RequestMapping("/Regular")
@RestController
public class RegularQueController {
    public Que que= new Que();
    public Ans ans=new Ans();

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private RegularQueService regularQueService;
    @PostMapping("/info")
    @Options(useCache = false)
    public Result info(@RequestBody String typejson){
//        return Result.ok();
        JSONObject jsonObject=JSON.parseObject(typejson);
        String type = jsonObject.getString("type");
        JSONArray data =new JSONArray();
        List<RegularQue> regularQueList = regularQueService.getByType(type);
        for (RegularQue regularQue :regularQueList) {
            JSONObject reg =new JSONObject();
            reg =StrtoJSONUtil.convertToJSON(regularQue.getAns());
            reg.put("type",regularQue.getType());
            reg.put("id",regularQue.getId());
            reg.put("que",regularQue.getQue());
            data.add(reg);
        }
        return Result.ok(data);
    }

    @GetMapping("/Answer")//这里放答案一共13道题  ans1是true 2是false
    public Result Answer(@RequestHeader("userId") String userId,@RequestParam("choose") Boolean choose){
//            System.out.println(userId+"\n"+choose);
        String token = "RQ:" + userId;
        // 序列化答案对象为JSON字符串
        String answerJson = JSON.toJSONString(choose);
        // 如果用户ID不存在，则创建新的答题记录
        // 存储答案到相应用户的答题记录中
        stringRedisTemplate.opsForList().rightPush(token, answerJson);
        //设置一个一天的缓存
        stringRedisTemplate.expire(token, 1, TimeUnit.DAYS);
        return Result.ok("AnswerNumber:"+stringRedisTemplate.opsForList().size(token));
    }
    @Resource
    private TestHistoryService testHistoryService;
    @Resource
    private TestResultService testResultService;
    //出结果 这里重写一下  把test输入到数据库里面 去操作来测试
    @GetMapping("/ComeOut")//这里type只有五大类型mood interpersonal Love growth career
    @Options(useCache = false)
    public Result ComeOut
    (@RequestHeader("userId") String userId,@RequestParam("type") String type)
    {
        String token = "RQ:" + userId;
        // 检查Redis中是否存在指定的userID
        boolean keyExists = stringRedisTemplate.hasKey(token);
        if (!keyExists){
            return Result.fail("你的测试记录已过期请重新测试！");
        }
        // 从Redis中获取答题记录
        List<String> answerJsons = stringRedisTemplate.opsForList().range(token, 0, -1);
        int score=0;
        for (String answerJson : answerJsons){
            //每一轮问题都会进行统计 answerJson里面只有true和false 如果是t就+1否则不加分
            // 将JSON字符串转换为Boolean值
            boolean answer = Boolean.parseBoolean(answerJson);
            if (answer) {
                score++;
            }
        }
//        TestResult test =new  TestResult(score,type);
        TestResult testResult = testResultService.getresult(score, type);
        JSONObject data=new JSONObject();
        data.put("text",JSONObject.parseObject(testResult.getText().replace("\\n","")));
        testResult.setText(null);
        data.put("testResult",testResult);
        data.put("time",LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        testHistoryService.putRegularQue(userId,type,JSONObject.toJSONString(data));
        return Result.ok(data);
    }

}

package TopOne.UserSystem.controller.Listener;


import TopOne.dto.Result;
import TopOne.ListenerSystem.service.ListenerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static TopOne.utils.RedisConstants.LOGIN_LISTENER_KEY;

@Slf4j
@RestController
@RequestMapping("/IsOnline")
public class IsOnline {

    @Resource
    private ListenerService listenerService;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @GetMapping("/Listener")
    public Result ListenerIsOnline(@RequestParam("token") String token){
        String listener = stringRedisTemplate.opsForValue().get(LOGIN_LISTENER_KEY+token);
        if (listener == null)
        {
            return Result.ok("false");
        }
        return Result.ok("true");
    }
}

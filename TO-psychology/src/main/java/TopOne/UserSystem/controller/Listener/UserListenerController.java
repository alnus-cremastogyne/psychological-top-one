package TopOne.UserSystem.controller.Listener;


import TopOne.ListenerSystem.entity.Listener;
import TopOne.ListenerSystem.service.ListenerService;
import TopOne.UserSystem.entity.Listener.Appointment;
import TopOne.dto.Result;
import TopOne.UserSystem.service.AppointmentService;
import TopOne.utils.RedisTable.ReservationService;
import cn.hutool.json.JSONArray;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Options;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@RestController
@RequestMapping("/user/Listener")
public class UserListenerController {
    //    上传预约表单信息
    @Resource
    private AppointmentService appointmentService;
    @Resource
    private ReservationService reservationService;
    @Resource
    private ListenerService listenerService;

    @GetMapping("/isOverTime")
    @Options(useCache = false)//判断能不能开启聊天通道也是这个接口
    public Result isOverTime(@RequestHeader ("UserId") String userId){
        //写一个方法根据userId获得 预约表单的信息
        Appointment appointment = appointmentService.queryByOpenId(userId);
        // 根据预约表单 获取time 预约具体时间字符串
        Date date = appointment.getAppointmentDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String DateStr = sdf.format(date);
        String DateAndTime =DateStr+"-"+appointment.getAppointmentTime();
        //此处的时间规格是yyyy-MM-dd-HH-mm
        boolean isOverTime = reservationService.isOverThatTime(DateAndTime);
        return Result.ok(isOverTime);
    }
//    @GetMapping("/isCanTalk")
//    @Options(useCache = false)
//    public Result isCanTalk(@RequestParam ("time") String time){//此处的时间规格是yyyy-MM-dd-HH-mm
//        //这里判断此时此刻的时间是否到达约定时间
//        boolean isOverTime = reservationService.isOverThatTime(time);
//        return Result.ok(isOverTime);
//    }
    @GetMapping("/GetUnReserved")
    @Options(useCache = false)//time:09:30-10:00 DateStr:2023-04-10
    public Result GetUnReservedList(@RequestParam("ListenerId")String ListenerId,@RequestParam ("time") String time,@RequestParam ("DateStr") String DateStr) {
        JSONArray UnreservedList = reservationService.GetUnReserved(ListenerId,DateStr);
        return Result.ok(UnreservedList);
    }
    @PostMapping("/upload-Appointment")
    @Options(useCache = false)
    public Result reserve(@RequestBody Appointment appointment){
        //先拼接yyyy-MM-dd-HH-mm格式的日期字符串
        Date date = appointment.getAppointmentDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String DateStr = sdf.format(date);
        String DateAndTime =DateStr+"-"+appointment.getAppointmentTime();
         if (!reservationService.isReserved(Integer.toString(appointment.getListenerId()),appointment.getAppointmentTime(),DateStr) && !reservationService.isOverThatTime(DateAndTime))
         {
            reservationService.Reserved(Integer.toString(appointment.getListenerId()),appointment.getAppointmentTime(),DateStr);
            //TODO 这里需要改预约表单 这个地方要注意
            appointmentService.uploadAppointment(appointment);
            return Result.ok("预约成功");
         }
         return Result.fail("预约失败");
    }
    @PostMapping("/delete-Appointment")
    public Result deleteAppointment(@RequestBody String json,@RequestHeader("userId") String user_id){
        JSONObject jsonObject = JSONObject.parseObject(json);
        int listener_id =jsonObject.getIntValue("listener_id");
        //这里要多 日期和时间段的参数  在这个方法里面要多加取消空闲临时表的方法
        return appointmentService.deleteAppointment(listener_id, user_id);
    }

    @GetMapping("/GetListenerList")//日期 和时间段
    @Options(useCache = false)
    public Result GetList(@RequestParam("date")String date,@RequestParam("time") String time)
    {
        //        return listenerService.test();
        //格式date 01-01 12-12    time 9-10 11-12
        return Result.ok(listenerService.GetListenerList(date,time));
    }

    @GetMapping("/GetListenerByAppointment")
    @Options(useCache = false)
    public Result GetListenerByAppointment(@RequestHeader("userId") String userId){
        //这里根据userid查倾听者信息 当聊天结束后
        // 就应该删除对应的表单，增加“倾听历史记录”
        Listener listener = appointmentService.getListenerByUserId(userId);
        if (listener==null) {
            return Result.fail("用户没有预约倾听者或者倾听者不存在");
        }
        return Result.ok(listener);
    }

    @GetMapping("/OverTalkWithListener")
    @Options(useCache = false)
    public Result OverTalkWithListener(@RequestHeader("userId") String userId,@RequestParam("listenerId") String listenerId){
        //这里是用户 直接 点击结束 结束了对话  删除对应预约表单 这里没做 增加历史倾听记录的操作
        return Result.ok(appointmentService.deleteAppointment(Integer.parseInt(listenerId),userId));
    }
    //查询倾听者是否同意//    @Resource
//    private StateCope
//    @GetMapping("/isAccept")
//    public Result isAccept(){
//
//    }

}








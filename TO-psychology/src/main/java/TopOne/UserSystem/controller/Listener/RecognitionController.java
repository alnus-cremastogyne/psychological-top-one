package TopOne.UserSystem.controller.Listener;

import TopOne.dto.Result;
import TopOne.utils.RecognitionUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class RecognitionController {

    //语音转文字
    @PostMapping("/recognize")
    public Result recognizeAudio(@RequestBody MultipartFile audio) throws IOException {
        //现在服务器很垃圾 支持 不了太多人的这些请求

        return Result.ok(RecognitionUtil.Recognition(audio.getInputStream()));
    }

}





















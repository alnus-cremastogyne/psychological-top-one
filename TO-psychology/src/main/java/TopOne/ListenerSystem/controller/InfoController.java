package TopOne.ListenerSystem.controller;


import TopOne.UserSystem.service.AppointmentService;
import TopOne.dto.Result;
import TopOne.ListenerSystem.entity.Listener;
import TopOne.ListenerSystem.service.ListenerService;
import TopOne.utils.ListenerHolder;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Options;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("/Listener/info")
public class InfoController {
    @Resource
    StringRedisTemplate stringRedisTemplate;
    @Resource
    private ListenerService ServiceListener;
    @PostMapping("/log")
    public Result log(@RequestBody String json){
        JSONObject jsonObject = JSONObject.parseObject(json);
        String  Account = jsonObject.getString("Account");
        String password = jsonObject.getString("Password");
        return ServiceListener.log(Account,password);
    }
    @GetMapping("/loginOut")
    public Result loginOut(@RequestHeader("token") String token){
        ServiceListener.logOut(token);
        return Result.ok();
    }
    //进入倾听者的“我的”的界面 获取到了倾听者的所有信息
    @GetMapping("/me")
    public Result Me(){
        //return Result.ok(ServiceListener.me());
        return Result.ok(ListenerHolder.getListener());
    }

    //这样在这里前端才知道他id
    @PostMapping("/upload-listener")
    public Result uploadListener(@RequestBody Listener listener, @RequestHeader("Listener_Id") int Listener_Id){
        return ServiceListener.uploadListener(listener);
    }
    //    修改倾听者部分信息
    @PostMapping("/update-listener")
    public Result updateListener(@RequestBody Listener updateListener,@RequestHeader("Listener_Id") int Listener_Id){
        return  ServiceListener.updateListener(updateListener);
    }

    //TODO
    @Options(useCache = false)
    @GetMapping("/GetVisitors")
    public Result GetVisitorsByDate(@RequestHeader("Listener_Id") String Listener_Id,@RequestParam("date") String date){
        //Date的格式是yyyy-mm-dd
        return ServiceListener.GetVisitors(Listener_Id,date);
    }
    @Resource
    private AppointmentService appointmentService;
//    @Options(useCache = false)
//    @GetMapping("/test")
//    public Result TTest(){//这个接口是个初始化预约临时表的接口
//        // 输入特定的指令就要调这个接口给后端程序初始化
//        ServiceListener.test();
//        return Result.ok();
//    }
    @Options(useCache = false)
    @GetMapping("/GetVisitorByid")
    public Result GetVisitorByid(@RequestParam("user_id")String userid){
        return Result.ok(ServiceListener.GetVisitorByid(userid));
    }
    //倾听者结束聊天的接口
    //要在消息处理器中记录已结束的状态 并且 1.把用户写的预约表单 写入预约历史记录中的forms字段 2.清空对应状态的缓存 3.删除预约表单


}

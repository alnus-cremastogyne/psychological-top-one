package TopOne.ListenerSystem.service.impl;

import TopOne.UserSystem.entity.Listener.Appointment;
import TopOne.UserSystem.service.AppointmentService;
import TopOne.UserSystem.service.IUserService;
import TopOne.dto.Result;
import TopOne.ListenerSystem.entity.Listener;
import TopOne.ListenerSystem.mapper.ListenerMapper;
import TopOne.ListenerSystem.service.ListenerService;
import TopOne.utils.RedisTable.ReservationService;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Options;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static TopOne.utils.RedisConstants.LOGIN_LISTENER_KEY;

/**
 * @author suze
 * @date 2023/11/8
 * @time 14:15
 **/
@Service
public class ListenerServiceImpl extends ServiceImpl<ListenerMapper, Listener> implements ListenerService
{
    @Resource
    private AppointmentService appointmentService;
    @Resource
    private IUserService userService;
//    @Resource
//    private ScheduleCleanupTask scheduleCleanupTask;
    @Resource
    private ReservationService reservationService;
    @Resource
    StringRedisTemplate stringRedisTemplate;
    @Override
    //这里判断的依据注意！用token
    public Result uploadListener(Listener listener){
        //这里需要倾听者的id给过来然后用倾听者的id保存
        updateById(listener);
        return Result.ok("倾听者资料上传成功");
    }

    @Override
    public Result updateListener(Listener listener)
    {
        if (updateById(listener)) {
            return Result.ok("Update success");
        }
            return Result.fail("fail");
    }

    @Override
    public Result log(String account, String password) {

        String token = RandomUtil.randomString(10) + account;
        Listener listener = query().eq("account", account).one();
        if (listener != null) {
            if (listener.getPassword().equals(password)) {
                //这里的token+LOGIN_LISTENER_KEY就相当于在线凭证 所以只要查询Redis里面有没有就能知道在线情况
                listener.setToken(token);
                stringRedisTemplate.opsForValue().set(LOGIN_LISTENER_KEY+token, JSONUtil.toJsonStr(listener),3, TimeUnit.DAYS);
                JSONObject data=new JSONObject();
                data.put("token",token);
                //这里记录了每一个号是否是第一次登录 如果是前端就要进入表单界面
                boolean isFirst=false;
                if (listener.getFirst().equals("true")){
                    isFirst=true;
                    //不是第一次了
                    listener.setFirst("false");
                }
                updateById(listener);
                listener.setAccount("Don't see!bad guy!");
                listener.setPassword("*********");
                data.put("listener",listener);
                data.put("IsFrist",isFirst);
                return Result.ok(data);
            }
        }
        return Result.fail("登录失败");
    }

    @Override
    public void logOut(@RequestHeader("token") String token) {
        String json = stringRedisTemplate.opsForValue().get(LOGIN_LISTENER_KEY+token);
        Listener listener = JSONObject.parseObject(json, Listener.class);
        //清空token
        listener.setToken("");
        updateById(listener);
        stringRedisTemplate.delete(LOGIN_LISTENER_KEY+token);
    }

    @Override
    @Options(useCache = false)
    public Result GetVisitors(String Listener_Id, String Time) {
        List<Appointment> appointment = appointmentService.getByListener(Integer.parseInt(Listener_Id),Time);
        return Result.ok(appointment);
    }

    @Override
    public Result GetVisitorByid(String userid) {
        Appointment appointment_lecture = appointmentService.queryByOpenId(userid);
        return Result.ok(appointment_lecture);
    }

    @Override
    public List<String> getListenerIdList() {
        List<Listener> listeners = query().list();
        List<String> ids = new ArrayList<>();
        for (Listener listener : listeners) {
            ids.add(listener.getId().toString());
        }
        return ids;
    }
    @Override
    public Integer getListenerId(String token) {
        String Key = "listener:" + "token:" + token;
        String json = stringRedisTemplate.opsForValue().get(Key);
        if (json!=null){
            Listener listener = JSONUtil.toBean(json, Listener.class);
            return listener.getId();
        }
        Listener listener = query().eq("token", token).one();
        stringRedisTemplate.opsForValue().set(Key,JSONUtil.toJsonStr(listener),1,TimeUnit.DAYS);
        return listener.getId();
    }

    @Override
    public List<Listener> GetListenerList(String date, String time) {
        List<Listener> listAll = query().list();
        List<Listener> list=new ArrayList<>();
        //没有登过号的就是还没被接管的倾听者 就不显示列表中
        for (Listener listener : listAll) {
            if (!listener.getFirst().equals("true")){
                if (listener.getFeatureTags()!=null){
                    listener.setFeatureTagArray(JSONUtil.parseArray(listener.getFeatureTags()));
                }
                list.add(listener);
            }
        }
//        LocalDate currentDate = LocalDate.now();
//        String dateStr = currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        //这里可以加判断空闲状态的逻辑
        for (Listener listener : listAll) {
            //根据倾听者的id查询 每一个倾听者对应的预约表 看看有没有空闲 有就显示
            if (reservationService.isReserved(Integer.toString(listener.getId()),time,date)) {
                //有被预约说明没空 移除列表
                list.remove(listener);
            }
        }

        return list;
    }

//    @Override
//    public cn.hutool.json.JSONObject me() {
//        Listener listener = getById(999);
//        if(listener == null){
//            return null;
//        }
//        cn.hutool.json.JSONObject jsonObject = new cn.hutool.json.JSONObject();
//        jsonObject.putOnce("listener",listener);
//        String tags = listener.getFeatureTags();
//        if (tags!= null){
//            JSONArray array = JSONUtil.parseArray(tags);
//            jsonObject.putOnce("featureTags",array);
//        }
//        else {
//            jsonObject.putOnce("featureTags","[]");
//        }
//        return jsonObject;
//    }

//    @Override
//    public Result test() {
//        scheduleCleanupTask.CreateBookTable();
//        return Result.ok();
//    }


}

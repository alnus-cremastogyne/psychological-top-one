package TopOne.ListenerSystem.service;

import TopOne.dto.Result;
import TopOne.ListenerSystem.entity.Listener;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author suze
 * @date 2023/11/8
 * @time 14:14
 **/

public interface ListenerService extends IService<Listener> {

    Result uploadListener(Listener listener);

    Result updateListener(Listener listener);

    Result log(String  account, String password);

    void logOut(String token);

    Result GetVisitors(String Listener_Id,String Time);

    Result GetVisitorByid(String userid);

    List<String> getListenerIdList();

    Integer getListenerId(String token);


    List<Listener> GetListenerList(String date,String time);

//    JSONObject me();

    //Result test();
}

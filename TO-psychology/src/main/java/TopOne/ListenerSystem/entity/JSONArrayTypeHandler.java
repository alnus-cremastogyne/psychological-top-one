package TopOne.ListenerSystem.entity;

import org.apache.ibatis.type.TypeHandler;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/***
 * @title JSONArrayTypeHandler
 * @author SUZE
 * @Date 13:20
 **/


@MappedTypes(JSONArray.class)
public class JSONArrayTypeHandler implements TypeHandler<JSONArray> {

    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, JSONArray jsonArray, JdbcType jdbcType) throws SQLException {
        preparedStatement.setString(i, jsonArray.toJSONString());
    }

    @Override
    public JSONArray getResult(ResultSet resultSet, String s) throws SQLException {
        return convertToJsonArray(resultSet.getString(s));
    }

    @Override
    public JSONArray getResult(ResultSet resultSet, int i) throws SQLException {
        return convertToJsonArray(resultSet.getString(i));
    }

    @Override
    public JSONArray getResult(CallableStatement callableStatement, int i) throws SQLException {
        return convertToJsonArray(callableStatement.getString(i));
    }

    private JSONArray convertToJsonArray(String fieldValue) {
        if (fieldValue == null || fieldValue.isEmpty()) {
            return new JSONArray();
        }
        return JSON.parseArray(fieldValue);
    }
}


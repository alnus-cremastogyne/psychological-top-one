package TopOne.ListenerSystem.entity;

import cn.hutool.json.JSONArray;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author suze
 * @date 2023/11/8
 * @time 14:10
 **/
@Data
@EqualsAndHashCode(callSuper = false)//不考虑父类字段
@Accessors(chain = true)//允许链式编程 setter和getter方法
@TableName("tb_listeners")
public class Listener implements Serializable {
    private static final long serialVersionUID = 2L;

    /**
     * 主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    //专门设置一个用来存放featureTag数组的变量 并且不映射到数据库
    @TableField(exist = false)
    private JSONArray featureTagArray;
    // 用户真实姓名，不能为空
    private String realName;

    // 用户昵称，可以为空
    private String nickname;

    // 用户电话号码，不能为空，唯一
    private String phone;

    // 用户性别，只能是'男'、'女'或'其他'
    private String gender;

    // 用户年龄，无符号小整数
    private Integer age;

    // 用户主修专业
    private String major;

    // 用户所在学校
    private String school;

    // 用户教育背景，可以是'小学'、'初中'、'高中'、'专科'、'本科'、'硕士'、'博士'或'其他'
    private String education;

    // 用户自我介绍
    private String selfIntroduction;

    // 用户所在地区
    private String region;

    private String  featureTags;

    // 用户人生座右铭
    private String lifeMotto;

    // 用户问题解决方法描述(解忧方向)
    private String troubleShooting;

    private String account;
    private String password;
    private String token;
    private String first;
    private String icon;



}

package TopOne.ListenerSystem.mapper;

import TopOne.ListenerSystem.entity.Listener;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author suze
 * @date 2023/11/8
 * @time 14:13
 **/
public interface ListenerMapper extends BaseMapper<Listener> {
}

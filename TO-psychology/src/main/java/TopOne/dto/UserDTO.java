package TopOne.dto;

import lombok.Data;
/*/**
 *@author suze
 *@date 2023-10-25
 *@time 15:19
 **/
@Data
public class UserDTO {
    private Long id;
    private String name;
    private String icon;
}

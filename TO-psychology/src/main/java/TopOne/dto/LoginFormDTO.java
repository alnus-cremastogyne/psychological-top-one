package TopOne.dto;

import lombok.Data;
/*/**
 *
 *@author suze
 *@date 2023-10-25
 *@time 15:12
 *
 **/
@Data
public class LoginFormDTO {
    private String phone;
    private String openId;
    private String sessionKey;
    private String name;
    private String icon;

}

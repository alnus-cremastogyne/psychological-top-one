package TopOne.dto;

import lombok.Data;

import java.util.List;
/*/**
 *@author suze
 *@date 2023-10-25
 *@time 15:19
 **/
@Data
public class ScrollResult {
    private List<?> list;
    private Long minTime;
    private Integer offset;
}

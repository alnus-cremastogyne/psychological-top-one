import Vue from 'vue'

import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
		//公共的变量，这里的变量不能随便修改，只能通过触发mutations的方法才能改变
		// 消息
		news:[],
		// 正在聊天的人的id
		liserId:0,
		// 正在聊天的人的数据
		liserDate:{},
		// 轮播图的数据
		onlinePeople:[],
		essay:[],
		// 文章详情的数据
		bookdate:{},
		// 文章列表
		booklist:[],
		// 讲座列表
		lecturesDate:[],
		// 讲座详情数据
		get_lecdate:{},
		// 问卷测试
		quesionTest:[],
		// 问卷测试数据
		exam:{},
		// mbei报告
		report1:{},
		reportText:"",
		report2:{},
		// 和谁聊天
		chatMessge: [{
			id: 0,
			belong: 1,
			content: "Hi，微信用户~欢迎回来,快来和我聊聊吧",
		}, {
			id: 1,
			belong: 1,
			content: "你有什么心事想和小A聊聊呢？你有什么心事想和小A聊聊呢？",
		}, ],
		// 我的 收藏列表
		collect:[],
		// 预约人的消息 
		liserDetail:{},
	},
    mutations: {
		//相当于同步的操作
		// 设置消息的数据
		SET_news_DATA:(state,newVal)=>{
			// console.log(what2)
			state.news.push({
				id: state.news.length,
				belong: newVal.iswho,
				content: newVal.what2,
			})
		},
		SET_liserDate_DATA:(state,newVal)=>{
			state.liserDate = newVal
		},
		SET_newsKong_DATA:(state,newVal)=>{
			// console.log(what2)
			state.news = []
		},
		// 设置在聊天的人的id的数据
		SET_liserId_DATA:(state,newVal)=>{
			state.liserId = newVal
		},
		// 设置在线人的数据
		SET_onlinePeople_DATA:(state,newVal)=>{
			state.onlinePeople = newVal
		},
		// 设置轮播图的数据
		SET_essay_DATA:(state,newVal)=>{
			state.essay = newVal
		},
		// 文章详情的数据
		SET_bookdate_DATA:(state,newVal)=>{
			state.bookdate = newVal
		},
		// 文章列表的数据
		SET_booklist_DATA:(state,newVal)=>{
			state.booklist = newVal
		},
		// 讲座列表
		SET_lecturesDate_DATA:(state,newVal)=>{
			state.lecturesDate = newVal
		},
		// 讲座详情
		SET_get_lecdate_DATA:(state,newVal)=>{
			state.get_lecdate = newVal
		},
		// 问卷测试
		SET_quesionTest_DATA:(state,newVal)=>{
			state.quesionTest = newVal
		},
		// 问卷测试题目
		SET_exam_DATA:(state,newVal)=>{
			state.exam = newVal
		},
		// MBTI报告
		SET_report1_DATA:(state,newVal)=>{
			state.report1 = newVal
		},
		SET_reportText_DATA:(state,newVal)=>{
			state.reportText = newVal
		},
		// 通用报告
		SET_report2_DATA:(state,newVal)=>{
			state.report2 = newVal
		},
		// 我的 收藏列表
		SET_collect_DATA:(state,newVal)=>{
			state.collect = newVal
		},
		// 预约人的信息
		SET_liserDetail_DATA:(state,newVal)=>{
			state.liserDetail = newVal
		},
	},
    actions: {
		//相当于异步的操作,不能直接改变state的值，只能通过触发mutations的方法才能改变
	}
})
export default store

import store from '../store/index.js'
class websocketUtil {
	constructor(url, time) {
		this.is_open_socket = false //避免重复连接
		this.url = url //地址
		this.data = null
		//心跳检测
		this.timeout= time //多少秒执行检测
		this.heartbeatInterval= null //检测服务器端是否还活着
		this.reconnectTimeOut= null //重连之后多久再次重连

		try {
			return this.connectSocketInit()
		} catch (e) {
			console.log('catch');
			this.is_open_socket = false
			this.reconnect();
		}
	}

	// 进入这个页面的时候创建websocket连接【整个页面随时使用】
	connectSocketInit() {
		let that = this
		this.socketTask = my.connectSocket({
			url: this.url,
			multiple: true, 
			success:()=>{
				console.log("正准备建立websocket中...");
				// 返回实例
				return this.socketTask
			},
		});
		this.socketTask.onOpen((res) => {
			console.log("WebSocket连接正常！");
			clearTimeout(this.reconnectTimeOut)
			clearTimeout(this.heartbeatInterval)
			this.is_open_socket = true;
			this.start();
			// 注：只有连接正常打开中 ，才能正常收到消息
			this.socketTask.onMessage((res) => {
				let data = JSON.parse(res.data)
				console.log(data)
				// console.log(uni.getStorageSync('openid'))
				if (data.users) { 
					// 获取在线人员信息
				store.commit('SET_onlinePeople_DATA', data.users.filter(user => user.userId !==
						uni.getStorageSync('openid')))
						console.log("在线人数为")
				console.log(store.state.onlinePeople)
				console.log("在线人数为")
					// _this.users = data.users.filter(user => user.username !==
					// 	username) // 获取当前连接的所有用户信息，并且排除自身，自己不会出现在自己的聊天列表里
				} else {
					// 如果服务器端发送过来的json数据 不包含 users 这个key，那么发送过来的就是聊天文本json数据
					//  // {"from": "zhang", "text": "hello"}
					// if (data.from === _this.chatUser) {
					// 	// _this.messages.push(data)
					// 	// 构建消息内容
					// 	_this.createContent(data.from, null, data.text)
					// }
				console.log("回调接受消息")
				console.log(res)
				
					// console.log("存入消息成功")
					// console.log(JSON.parse(res.data))
					// console.log("存入消息成功")
					let messge = JSON.parse(res.data)
				store.commit('SET_news_DATA',{'iswho':1, 'what2':messge.text})
				console.log(store.state.news)
				}
				
				
				
			});
		})
		// 监听连接失败，这里代码我注释掉的原因是因为如果服务器关闭后，和下面的onclose方法一起发起重连操作，这样会导致重复连接
		// uni.onSocketError((res) => {
		// 	console.log('WebSocket连接打开失败，请检查！');
		// 	this.is_open_socket = false;
		// 	this.reconnect();
		// });
		// 这里仅是事件监听【如果socket关闭了会执行】
		this.socketTask.onClose(() => {
			console.log("已经被关闭了")
			this.is_open_socket = false;
			this.reconnect();
		})
	}
	
	//发送消息
	send(value){
		// 注：只有连接正常打开中 ，才能正常成功发送消息
		this.socketTask.send({
			data: value,
			async success() {
				console.log("消息发送成功");
			},
		});
	}
	//开启心跳检测
	start(){
		this.heartbeatInterval = setTimeout(()=>{
			this.data={"from": uni.getStorageSync('openid'),
					"to": 'test123',
					"text": "心跳测试"}
			console.log(this.data)
			this.send(JSON.stringify(this.data));
		},this.timeout)
	}
	//重新连接
	reconnect(){
		//停止发送心跳
		clearInterval(this.heartbeatInterval)
		//如果不是人为关闭的话，进行重连
		if(!this.is_open_socket){
			this.reconnectTimeOut = setTimeout(()=>{
				this.connectSocketInit();
			},3000)
		}
	}
	//外部获取消息
	getMessage(callback) {
		this.socketTask.onMessage((res) => {
			return callback(res)
		})
	}
 
}

module.exports = websocketUtil


// 全局调用
// 在main.vue页面中
//引入websocket文件
// import wsRequest from './static/js/websocket.js'
// //开启websocket
// let websocket = new wsRequest("ws://xxx:3100/connect/websocket",5000)
// //挂载到全局
// Vue.prototype.$socket = websocket

// 页面中调用
// 发送消息
// let data={value:"传输内容",method:"方法名称"}
// this.$socket.send(JSON.stringify(data));

// // 接收消息
// this.$socket.getMessage(res=>{
// 	console.log(res);
// })

// //在测试环境时url可以写成  ws://xxx:3100/connect/websocket
// new wsRequest("ws://xxx:3100/connect/websocket",5000)

// //发布体验版或正式版，url一定要写成  wss://xxx:3100/connect/websocket
// new wsRequest("wss://xxx:3100/connect/websocket",5000)



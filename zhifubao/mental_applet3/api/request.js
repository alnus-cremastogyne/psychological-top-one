import config from './config.js';
let that = this;
const request = (url = '', method = 'GET', data = {}, header = {   //这里这样封装是为了后续具体组件中使用时可以直接传参,需按此顺序传参；而不需要写url：xxx等键值对传参
	//具体的header和后端商同后再编写，这里以常见的token为例
	'token': uni.getStorageSync('token') ? uni.getStorageSync('token') : '1',
	'token2': uni.getStorageSync('token2') ? uni.getStorageSync('token2') : '1',
	// 'token': uni.getStorageSync('token'),
	'userId': uni.getStorageSync('openid'),
	'listener_Id':uni.getStorageSync('listener_Id'),
}) => {
	return new Promise((resolve, reject) => {
		uni.request({
			url: config.api() + url, //接口地址：前缀+方法中传入的地址
			method: method, //请求方法
			data: data, //传递参数
			header: header, //自定义头部，和后端商同后编写
			success: (res) => {
				console.log('request.js文件的通用接口请求封装返回的结果数据',res);
				//注：因为这里对请求成功的没有统一设置抛错提示，所以后续具体组件中使用接口请求的res除200（实际以后端同事定好的为准）成功外的其他code需要额外写抛错提示
				if (res.statusCode == 401) { //自定请求失败的情况，这里以常见的token失效或过期为例
					// uni.removeStorageSync('token');
					console.log("登录已失效，请登录")
					uni.showModal({
						showCancel: false,
						title: '登录已失效，请登录',
						// content: ,
						icon: 'none',
						success: function(result) {
							if (result.confirm) {
								// uni.reLaunch({
								// 	url: '/pages/login/index'    //这里需用绝对路径才可
								// });
								// 强制跳转到登录页
								// that.$goBack(2,'/pages/login/login');
								uni.navigateTo({
									url: '/pages/login/login',
									animationType: 'pop-in',
									animationDuration: 300
								})
							}
						}
					});
				}
				resolve(res.data) //成功
			},
			// 这里的接口请求，如果出现问题就输出接口请求失败的msg；
			//注：因为这里对于请求失败的设置统一抛错提示了，所以后续具体组件中使用接口请求的catch中不需要再写抛错提示
			fail: (err) => {
				// uni.showToast({
				// 	title: "" + err.msg,
				// 	icon: 'none'
				// });
				if (err.statusCode == "401") { //自定请求失败的情况，这里以常见的token失效或过期为例
					// uni.removeStorageSync('token');
					console.log("登录已失效，请重新登录")
					uni.showModal({
						showCancel: false,
						title: '请先登陆',
						// content: ,
						icon: 'none',
						success: function(result) {
							if (result.confirm) {
								// uni.reLaunch({
								// 	url: '/pages/login/index'    //这里需用绝对路径才可
								// });
								// 强制跳转到登录页
								// that.$goBack(2,'/pages/login/login');
								uni.navigateTo({
									url: '/pages/login/login',
									animationType: 'pop-in',
									animationDuration: 300
								})
							}
						}
					});
				}
				if (err.statusCode == "500") { //自定请求失败的情况，这里以常见的token失效或过期为例
					// uni.removeStorageSync('token');
					console.log("您的网络开了点小差喔")
					uni.showModal({
						showCancel: false,
						title: '您的网络开了点小差喔',
						// content: ,
						icon: 'none',
					});
				}
				
				
				
				
				
				
				reject(err)
				console.log("err")
				console.log(err)
			}
		})
	})
}

export default request;

export default {
	//接口请求域名配置
	api: function() {
		let curVersion = uni.getAccountInfoSync().miniProgram.envVersion;
		switch (curVersion) {
			case "develop": //开发版127.0.0.1
				// return "http://192.168.114.138:8081/";
				// return "http://112.74.50.188:8081/";
				// 公网
				return "https://to-one-38f1efae-ecb6-403a-a516-8d2baf4a94d1.dev-hz.cloudbaseapp-sandbox.cn/";
			case 'trial': //体验版
				return "https://to-one-38f1efae-ecb6-403a-a516-8d2baf4a94d1.dev-hz.cloudbaseapp-sandbox.cn/";
			case 'release': //正式版
				return "https://to-one-38f1efae-ecb6-403a-a516-8d2baf4a94d1.dev-hz.cloudbaseapp-sandbox.cn/"
			default: //其他,默认调用正式版
				return "https://to-one-38f1efae-ecb6-403a-a516-8d2baf4a94d1.dev-hz.cloudbaseapp-sandbox.cn/"
		}
	}
}


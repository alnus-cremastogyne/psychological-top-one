import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import './uni.promisify.adaptor'
// uView组件的引入
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)

// 引入跳转页面的方法
import { goBack } from 'utils/common.js' //公共方法
Object.assign(Vue.prototype, {
	'$goBack':goBack,
})

// 请求接口封装的入口
import request from './api/request.js';
Vue.prototype.$request = request

//引入websocket文件
import wsRequest from 'utils/websocket.js'


//解决刚刚登录的时候没openid
function loginSuccessCallback(){
	console.log(111)
if (!Vue.prototype.$socket) {
	//开启websocket
	console.log(222)
let websocket = new wsRequest("ws//to-one-38f1efae-ecb6-403a-a516-8d2baf4a94d1.dev-hz.cloudbaseapp-sandbox.cn/:8081/imserver/"+uni.getStorageSync('openid'),5000)
//挂载到全局
Vue.prototype.$socket = websocket	
}
}
Vue.prototype.loginSuccessCallback = loginSuccessCallback




if(uni.getStorageSync('openid')){
	loginSuccessCallback()
	console.log(333)
}



// vuex仓库的建立
import store from './store/index.js'
Vue.prototype.$store = store

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	store,
  ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif